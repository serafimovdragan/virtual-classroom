<!DOCTYPE html>
<html>
<head>
    <title>Virtual Clasroom - Examinations</title>
    <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="startbootstrap-sb-admin-gh-pages/css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.jsp">Home</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user"></i>
                <text id="loggedName"></text>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a class="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="form-group">
            <label>Examination Name:</label>
            <label id="examination-name"></label>
        </div>
        <div class="form-group">
            <label>Examination Type:</label>
            <label id="examination-type"></label>
        </div>
        <div class="form-group">
            <label>Examination Instance Name:</label>
            <label id="instance-name-label"></label>
        </div>
        <div id="questions">
        </div>
    </div>
</div>
<script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
<script src="js/ajax.js"></script>
<script src="js/authentication.js"></script>
<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery-ui.1.11.2.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<script type="application/javascript">
    var examination;

    $(document).ready(function() {

        var hosts = ['Professor', 'Assistant', 'Student', 'None'];
        redirect(hosts);

        fillNavBar($('#loggedName'));
        var examinationId = {
            "id":getUrlVars()['id']
        };

        ajaxGet('examination/get', examinationId, null, renderExamination);

        $('body').on('click', 'a.logout', function(e) {
            logout();
            e.preventDefault();
        });
        $('body').on('click', 'button#create-instance', function() {

            var instanceName = $('#examination-instance-name').val();

            localStorage.setItem('instanceName', instanceName);
            localStorage.setItem('courseId', getUrlVars()['id']);
            if(instanceName !== '') {
                var instance = {
                    "id" : getUrlVars()['id'],
                    "instance" : instanceName,
                    "professorId" : JSON.parse(localStorage.getItem("loggedUserName"))[0].id
                };

                ajaxGet('examination/activate/', instance, null, redirectExamInstance);
            } else {

            }
        });

        $('body').on('click', 'button.enable-question', function() {

            var questionInstance = {
                "examinationId" : getUrlVars()['id'],
                "instance" : localStorage.getItem("instanceName"),
                "questionId" : $(this).parent().prev().attr("id"),
                "duration" : $(this).prev().val()
            };

            ajaxGet("examination/question/activate", questionInstance, null, disableQuestions);
        });


    });

    function disableQuestions() {

        var instance = {
            "id": getUrlVars()['id'],
            "instance": getUrlVars()['instance']
        };
        ajaxGet("examination/instance/get", instance, null, doDisableQuestions);
    }

    function doDisableQuestions(data) {
        for(var i = 0; i < data.questions.length; i ++) {
            $("#" + data.questions[i].questionId).next().find("button").attr('disabled','disabled');
        }
    }

    function redirectExamInstance() {

        var courseId = localStorage.getItem('courseId');
        var instanceName = localStorage.getItem('instanceName');
        window.location = 'view_examination.jsp?id=' + courseId + '&instance=' + instanceName;
    }

    function renderExamination(data) {
        $('#examination-name').html(data.name);
        $('#examination-type').html(data.type);
        $('#instance-name-label').html(localStorage.getItem('instanceName'));
        $('#examination-instance-name').val(getUrlVars()['instance']);

        for(var i = 0; i < data.questions.length; i++) {
            var question = data.questions[i];

            if(question.id != getUrlVars()['question']) {
                continue;
            }

            var answers = '';
            for(var j = 0; j < question.answers.length; j++) {
                var answer = question.answers[j];

                if(question.type === 'Checkbox') {
                	var checked = '';
                    if(answer.isCorrect) {
						checked = 'checked';
                    }
                    answers += '<div class="checkbox"> <label id="' + answer.id + '"> <input type="checkbox" disabled '+ checked +' value="">' + answer.answer + '</label> </div>';
                } else if(question.type === 'Radio') {
                	var checked = '';
                    if(answer.isCorrect) {
						checked = 'checked';
                    }
                    answers += '<div class="radio"> <label id="' + answer.id + '"> <input type="radio" disabled '+ checked +' name="optionsRadios">' + answer.answer + '</label> </div>';
                } else if(question.type == "Fill") {
                    answers += '<textarea class="form-control" rows="3" placeholder="Your Answer" disabled>' + answer.answer + '</textarea>';
                }
            }

            if(question.type === 'Picture') {
                answers += '<canvas id="canvas' + question.id + '"></canvas>';

                answers += '<img id="picture' + question.id + '" src="../../' + question.pictureURL + '" alt="Image preview...">';
            }



            $('#questions').append('<div id="' + question.id + '" class="form-group ' + question.type + '"> <h1 class="name">' + question.question + '</h1>' + answers + ' <input id="' + question.id + '" class="hidden" type="hidden"></input> </div> </div>');
            $('#questions').append('<div id="resizable" style="height: 370px;border:1px solid gray;">\n' +
                '\t<div id="chartContainer1" style="height: 100%; width: 100%;"></div>\n' +
                '</div>');

            if(question.type === 'Picture') {
                var canvas = $("#canvas" + question.id);
                canvas.css("background-image", "url(../../" + question.pictureURL + ")");
                var picture = $("#picture" + question.id);
                var height = picture.height();
                var width = picture.width();

                canvas.height(height);
                canvas.width(width);
                picture.remove();
            }

            var questionParam = {
                "instance": getUrlVars()['instance'],
                "id": getUrlVars()['question']
            };
            ajaxGet("examination/question/results", questionParam, null, renderResults);
        }
    }

    function renderResults(data) {
        if(data.type == 'Radio') {
            var dataPoints = [];
            for(var i = 0; i < data.answers.length; i++) {
                var dataPoint = {};

                dataPoint.label = data.answers[i].answer;
                dataPoint.answerId = data.answers[i].id;
                dataPoint.y = 0;

                dataPoints.push(dataPoint);
            }

            for(var i = 0; i < data.submittedAnswers.length; i++) {
                for(var j = 0; j < dataPoints.length; j++) {
                    if(dataPoints[j].answerId == data.submittedAnswers[i].answerId) {
                        dataPoints[j].y++;
                    }
                }
            }

            var options1 = {
                animationEnabled: true,
                title: {
                    text: "Results"
                },
                data: [{
                    type: "column", //change it to line, area, bar, pie, etc
                    legendText: "Try Resizing with the handle to the bottom right",
                    showInLegend: false,
                    dataPoints: dataPoints
                }]
            };

            $("#resizable").resizable({
                create: function (event, ui) {
                    //Create chart.
                    $("#chartContainer1").CanvasJSChart(options1);
                },
                resize: function (event, ui) {
                    //Update chart size according to its container size.
                    $("#chartContainer1").CanvasJSChart().render();
                }
            });
        } else if (data.type == "Checkbox") {
            var dataPoints = [];
            for(var i = 0; i < data.answers.length; i++) {
                var dataPoint = {};

                dataPoint.label = data.answers[i].answer;
                dataPoint.answerId = data.answers[i].id;
                dataPoint.y = 0;

                dataPoints.push(dataPoint);
            }

            for(var i = 0; i < data.submittedAnswers.length; i++) {
                for(var j = 0; j < dataPoints.length; j++) {
                    if(dataPoints[j].answerId == data.submittedAnswers[i].answerId) {
                        dataPoints[j].y++;
                    }
                }
            }

            var options1 = {
                animationEnabled: true,
                title: {
                    text: "Results"
                },
                data: [{
                    type: "column", //change it to line, area, bar, pie, etc
                    legendText: "Try Resizing with the handle to the bottom right",
                    showInLegend: false,
                    dataPoints: dataPoints
                }]
            };

            $("#resizable").resizable({
                create: function (event, ui) {
                    //Create chart.
                    $("#chartContainer1").CanvasJSChart(options1);
                },
                resize: function (event, ui) {
                    //Update chart size according to its container size.
                    $("#chartContainer1").CanvasJSChart().render();
                }
            });
        } else if (data.type == "Fill") {
            var dataPoints = [];

            for(var i = 0; i < data.submittedAnswers.length; i++) {
                var found = false;
                for(var j = 0; j < dataPoints.length; j++) {
                    if(dataPoints[j].label == data.submittedAnswers[i].answer) {
                        dataPoints[j].y++;
                        found = true;
                    }
                }
                if(!found) {
                    var dataPoint = {};

                    dataPoint.label = data.submittedAnswers[i].answer;
                    dataPoint.y = 1;

                    dataPoints.push(dataPoint);
                }
            }

            var options1 = {
                animationEnabled: true,
                title: {
                    text: "Results"
                },
                data: [{
                    type: "column", //change it to line, area, bar, pie, etc
                    legendText: "Try Resizing with the handle to the bottom right",
                    showInLegend: false,
                    dataPoints: dataPoints
                }]
            };

            $("#resizable").resizable({
                create: function (event, ui) {
                    //Create chart.
                    $("#chartContainer1").CanvasJSChart(options1);
                },
                resize: function (event, ui) {
                    //Update chart size according to its container size.
                    $("#chartContainer1").CanvasJSChart().render();
                }
            });
        } else if (data.type == "Picture") {

        }
    }

    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
</script>
</body>
</html>