function setLoggedUser(loggedUser) {
    localStorage.setItem('loggedUser', JSON.stringify(loggedUser));

    if(loggedUser.professorId) {

        var professorId = {
            "id": loggedUser.professorId
        };
        ajaxGet('admin/professor/get', professorId, null, setLoggedUserName)
    } else if(loggedUser.studentId) {

        var studentId = {
            "id": loggedUser.studentId
        };
        ajaxGet('admin/student/get', studentId, null, setLoggedUserName)
    } else if(loggedUser.adminId) {

        var adminId = {
            "id": loggedUser.adminId
        };
        ajaxGet('admin/admin/get', adminId, null, setLoggedUserName)
    }
}

function setLoggedUserName(loggedUser) {
    localStorage.setItem('loggedUserName', JSON.stringify(loggedUser));
}

function redirect(hosts) {

    var loggedUser = JSON.parse(localStorage.getItem('loggedUserName'));

    if(!loggedUser) {
        for(var i = 0; i < hosts.length; i ++) {
            if(hosts[i] === 'None') {
                return;
            }
        }
    }

    var type = '';

    if(loggedUser[0].type) {
        type = loggedUser[0].type;
    } else if(loggedUser[0].credits) {
        type = 'Student';
    } else {
        type = 'Admin';
    }

    var needsRedirect = true;

    for(var i = 0; i < hosts.length; i ++) {
        if(hosts[i] === type) {
            needsRedirect = false;
        }
    }

    if(needsRedirect === false) {
        return;
    }

    if(type === 'Professor' || type === 'Assisstant') {
        window.location = 'examinations.jsp';
    } else if(type === 'Student') {
        window.location = 'index.jsp';
    } else if(type === 'Admin') {
        window.location = 'register.jsp';
    }
}

function fillNavBar(nameHolder) {
    var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));
    var loggedUserName = JSON.parse(localStorage.getItem('loggedUserName'));

    if(loggedUser && loggedUserName) {
        nameHolder.html(loggedUserName[0].name + ' ' + loggedUserName[0].surname);
    }
}

function logout() {
    var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));
    var loggedUserName = JSON.parse(localStorage.getItem('loggedUserName'))[0];

    if(loggedUser && loggedUserName) {
        if(loggedUser.studentId) {
            ajaxPost("auth/student/logout", null, loggedUser.session, clearLocalStorage, "login.jsp", clearLocalStorage, "login.jsp");
            window.location = "index.jsp";
        } else if(loggedUser.adminId) {
            ajaxPost("auth/admin/logout", null, loggedUser.session, clearLocalStorage, "login.jsp", clearLocalStorage, "login.jsp");
            window.location = "login.jsp";
        } else if(loggedUser.professorId) {
            ajaxPost("auth/professor/logout", null, loggedUser.session, clearLocalStorage, "login.jsp", clearLocalStorage, "login.jsp");
        }
    }
}

function clearLocalStorage() {
    localStorage.clear();
}