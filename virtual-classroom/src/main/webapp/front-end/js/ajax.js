function ajaxPost(URL, data, token, successFunction, successLocation, errorFunction, errorLocation) {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/virtual-classroom-0.0.1-SNAPSHOT/api/" + URL,
        crossDomain: true,
        contentType: "application/json",
        async: false,
        headers: {
            "Authorization" : token
        },
        accepts: {
            "Access-Control-Allow-Headers" : "x-requested-with, x-requested-by",
            "Access-Control-Request-Headers": "x-requested-with",
            "Access-Control-Allow-Origin": "*"
        },
        data: data,
        success: function(data){
            successFunction(data);
            if(successLocation) {
                window.location = successLocation;
            }
        },
        error: function(error) {
            errorFunction(error);
            if(errorLocation) {
                window.location = errorLocation;
            }
        }
    });
}

function ajaxGet(URL, data, token, successFunction, successLocation, errorLocation) {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/virtual-classroom-0.0.1-SNAPSHOT/api/" + URL,
        crossDomain: true,
        contentType: "application/json",
        async: false,
        headers: {
            "Authorization" : token
        },
        accepts: {
            "Access-Control-Allow-Headers" : "x-requested-with, x-requested-by",
            "Access-Control-Request-Headers": "x-requested-with"
        },
        data: data,
        success: function(data){
            if(jQuery.isFunction(successFunction)) {
                successFunction(data);
            }
            
            if(successLocation) {
                window.location = successLocation;
            }
        },
        error: function(error) {
            alert(error.responseJSON.msg);
            if(errorLocation) {
                window.location = errorLocation;
            }
        }
    });
}

function ajaxPostUpload(URL, data, token, successFunction, successLocation, errorFunction, errorLocation) {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/virtual-classroom-0.0.1-SNAPSHOT/api/" + URL,
        processData: false,
        contentType: false,
        data: data,
        success: function(data){
            successFunction(data);
            if(successLocation) {
                window.location = successLocation;
            }
        },
        error: function(error) {
            errorFunction(error);
            alert(error.responseJSON.msg);
            if(errorLocation) {
                window.location = errorLocation;
            }
        }
    });
}