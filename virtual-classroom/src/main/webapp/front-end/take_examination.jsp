<!DOCTYPE html>
<html>
<head>
    <title>Virtual Clasroom - Examinations</title>
    <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="startbootstrap-sb-admin-gh-pages/css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.jsp">Home</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user"></i>
                <text id="loggedName"></text>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li> <a class="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a> </li>
            </ul>
        </li>
    </ul>
</nav>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="form-group">
            <label>Course:</label>
            <label id="course-name">Kurs</label>
        </div>
        <div class="form-group">
            <label>Examination Name:</label>
            <label id="examination-name">Tes</label>
        </div>
        <div id="questions-div">
            <div id="questions">

            </div>
        </div>
    </div>
</div>
<script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
<script src="js/ajax.js"></script>
<script src="js/authentication.js"></script>
<script src="js/jquery.countdown-2.2.0/jquery.countdown.js"></script>
<script src="js/jquery.countdown-2.2.0/jquery.countdown.min.js"></script>
<script type="application/javascript">

    var examination;

    $(document).ready(function() {

        // setTimeout(function(){
        //     window.location.reload(1);
        // }, 5000);

        var hosts = ['Student', 'None'];
        redirect(hosts);
        window.localStorage.removeItem("examination");

        var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));
        var loggedUserName = JSON.parse(localStorage.getItem('loggedUserName'));
        if(loggedUserName) {
            loggedUserName = loggedUserName[0];
            fillNavBar($('#loggedName'));
        } else {
            $(".top-nav").html('<li class="dropdown"> <a href="login.jsp"><i class="fa fa-user"></i> Login <b class="caret"></b> </li>');
        }

        $('body').on('click', 'canvas', function(e) {
            var offset = $(this).offset();
            var X = e.pageX - offset.left;
            var Y = e.pageY - offset.top;

            drawCircle(this, X, Y);
        });

        function drawCircle(canvas, X, Y) {
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = "#0000FF";
            context.beginPath();
            context.fillRect(X, Y, 10, 10);
            context.stroke();
            context.fill();

            canvas.prev().attr("id", X + "," + Y);
        }

        $('body').on('click', 'a.logout', function(e) {
            logout();
            e.preventDefault();
        });
        var examinationId = {
            "name":getUrlVars()['examination']
        };

        examinationId.name = decodeURI(examinationId.name);
        ajaxGet('student/examination', examinationId, null, renderExamination);

        $('body').on('click', '.questions li a', function(event) {
            $('.questions li').removeClass('active');
            $(this).parent().addClass('active');
            $(this).parent().css('backgroundColor', '#90EE90');

            $('#questions').children().each(function () {
                $(this).hide();
            });
            var divId = 'q' + $(this).attr('id');

            $('#' + divId).show();
            event.preventDefault();
        });

        $('body').on('click', 'button.view-results', function() {

            var courseId = localStorage.getItem('courseId');
            var instanceName = localStorage.getItem('instanceName');

            window.location = 'view_results.jsp?id=' + examination.id + '&instance=' + getUrlVars()['examination'] + '&question=' + $(this).parent().prev().attr('id');
        });

        $('body').on('click', '.submit-answer', function() {
            var submittedExamination = [];

            if($(this).parent().parent().hasClass('Checkbox')) {

                var answerIds = [];

                $(this).parent().parent().find('input:checked').each(function() {
                    answerIds.push($(this).parent().attr('id'));
                });

                for(var i = 0; i < answerIds.length; i++) {
                    var submittedAnswer = {};

                    submittedAnswer.examinationId = examination.id;
                    submittedAnswer.questionId = $(this).parent().parent().find('.hidden').attr('id');
                    submittedAnswer.instance = getUrlVars()["examination"];
                    if(loggedUserName) {
                        submittedAnswer.studentId = loggedUserName.id;
                    }

                    submittedAnswer.questionType = 'Checkbox';
                    submittedAnswer.answerId = answerIds[i];
                    submittedExamination.push(submittedAnswer);
                }

            } else if($(this).parent().parent().hasClass('Radio')) {
                var submittedAnswer = {};

                submittedAnswer.examinationId = examination.id;
                submittedAnswer.questionId = $(this).parent().parent().find('.hidden').attr('id');
                submittedAnswer.instance = getUrlVars()["examination"];
                if(loggedUserName) {
                    submittedAnswer.studentId = loggedUserName.id;
                }

                submittedAnswer.questionType = 'Radio';
                submittedAnswer.answerId = $(this).parent().parent().find('input:checked').parent().attr('id');

                submittedExamination.push(submittedAnswer);
            } else if($(this).parent().parent().hasClass('Fill')) {
                var submittedAnswer = {};

                submittedAnswer.examinationId = examination.id;
                submittedAnswer.questionId = $(this).parent().parent().find('.hidden').attr('id');
                submittedAnswer.instance = getUrlVars()["examination"];
                if(loggedUserName) {
                    submittedAnswer.studentId = loggedUserName.id;
                }

                submittedAnswer.questionType = 'Fill';
                submittedAnswer.answer = $(this).parent().parent().find('textarea').val();

                submittedExamination.push(submittedAnswer);
            } else if($(this).parent().parent().hasClass('Picture')) {
                var submittedAnswer = {};

                submittedAnswer.examinationId = examination.id;
                submittedAnswer.questionId = $(this).parent().parent().find('.hidden').attr('id');
                submittedAnswer.instance = getUrlVars()["examination"];
                if(loggedUserName) {
                    submittedAnswer.studentId = loggedUserName.id;
                }

                submittedAnswer.questionType = 'Picture';

                submittedExamination.push(submittedAnswer);
            }

            ajaxPost("student/examination/submit", JSON.stringify(submittedExamination), null, examinationSubmitted, null, examinationSubmitError);
        });
    });

    function examinationSubmitted(data) {
       //$("#" + data).find(".submit-answer").attr("disabled", "disabled");
    }

    function examinationSubmitError(data) {
        alert(data.responseJSON.msg);
    }

    function renderExamination(data) {
        examination = data;

        $('#course-name').html(data.courseId);
        $('#examination-name').html(data.name);

        for(var i = 0; i < data.questions.length; i++) {
            var question = data.questions[i];

            var answers = '';
            for(var j = 0; j < question.answers.length; j++) {
                var answer = question.answers[j];

                if(question.type === 'Checkbox') {
                    answers += '<div class="checkbox"> <label id="' + answer.id + '"> <input type="checkbox" value="">' + answer.answer + '</label> </div>';
                } else if(question.type === 'Radio') {
                    answers += '<div class="radio"> <label id="' + answer.id + '"> <input type="radio" name="optionsRadios">' + answer.answer + '</label> </div>';
                }
            }

            if(question.type === 'Fill') {
                answers += '<textarea class="form-control" rows="3" placeholder="Your Answer"></textarea>';
            } else if(question.type === 'Picture') {
                answers += '<canvas id="canvas' + question.id + '"></canvas>';

                answers += '<img id="picture' + question.id + '" src="../../' + question.pictureURL + '" alt="Image preview...">';

            }

            $('#questions').append('<div id="' + question.id + '" class="form-group ' + question.type + '"> <h1 class="name">' + question.question + '<span id="countdown-timer' + question.id + '"></span></h1>' + answers + ' <hr><input id="' + question.id + '" class="hidden" type="hidden"></input> <div class="form-group"> <button type="submit" class="btn btn-primary submit-answer">Submit Answer</button> </div> </div> </div>')

            if(question.type === 'Picture') {
                var canvas = $("#canvas" + question.id);
                canvas.css("background-image", "url(../../" + question.pictureURL + ")");
                var picture = $("#picture" + question.id);
                var height = picture.height();
                var width = picture.width();

                canvas.height(150);
                canvas.width(300);
                picture.remove();
            }

            var expirationDate = question.expirationDate;
            if(!expirationDate) {
                expirationDate = "2010/01/01";
                $("div #" + question.id + " button").prop("disabled",true);
                $('#countdown-timer' + question.id).html(' Question expired!');
            } else {
                $('#countdown-timer' + question.id).countdown(expirationDate, function(event) {
                    $(this).html(event.strftime(' %H:%M:%S'));
                }).on('finish.countdown', function(event) {

					var questionInstance = {
						"instance": getUrlVars()["examination"],
						"questionId": $(this).parent().parent().attr('id')
					}
                    
                    ajaxGet("examination/question/isPublic", questionInstance, null, showResultsButton);
                    $("div #" + question.id + " button").prop("disabled",true);
                    $(this).html(event.strftime(' Question expired!'));
                });
            }

            var showResultsData = {
				"id": question.id,
				"seeResults": question.seeResults
            };
            showResultsButton(showResultsData);
        }
    }

    function showResultsButton(data) {
		if(data.seeResults == true) {
			$('#' + data.id).find('.submit-answer').parent().append('<button type="submit" class="btn btn-primary view-results">View Results</button>');
		}
    }

    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
</script>
</body>
</html>