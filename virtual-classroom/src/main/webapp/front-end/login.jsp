<!DOCTYPE html>
<html>
   <head>
      <title>Virtual Clasroom - Login</title>
      <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
      <!-- Custom Fonts -->
      <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   </head>
   <body>
      <div id="page-wrapper">
         <div class="container-fluid">
            <div class="col-lg-4"></div>
            <!-- Page Heading -->
            <div class="col-lg-4">
               <div class="row">
                  <div class="col-lg-12">
                     <h1 class="page-header">
                        Login
                     </h1>
                  </div>
               </div>
               <!-- /.row -->
               <!--<div>
                  <label>Student Login</label>
                  <input id="student-username" class="form-control" placeholder="Username">
                  <input id="student-password" type="password" class="form-control" placeholder="Password">
                  <button id="student-submit" type="submit" class="btn btn-primary">Student Login</button>
               </div>-->
               <hr>
               <div>
                  <label>Professor Login</label>
                  <input id="professor-username" class="form-control" placeholder="Username">
                  <input id="professor-password" type="password" class="form-control" placeholder="Password">
                  <button id="professor-submit" type="submit" class="btn btn-primary">Professor Login</button>
                  <button id="professor-register" type="submit" class="btn btn-primary">Professor Register</button>
               </div>
               <hr>
               <!--<div>
                  <label>Admin Login</label>
                  <input id="admin-username" class="form-control" placeholder="Username">
                  <input id="admin-password" type="password" class="form-control" placeholder="Password">
                  <button id="admin-submit" type="submit" class="btn btn-primary">Admin Login</button>
               </div>-->
            </div>
            <div class="col-lg-4"></div>
         </div>
         <!-- /.container-fluid -->
      </div>
      <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
      <!-- Morris Charts JavaScript -->
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
      <script src="js/ajax.js"></script>
      <script src="js/authentication.js"></script>
      <script type="application/javascript">
          $(document).ready(function() {

              var hosts = ['None'];
              redirect(hosts);

/*              $("button#admin-submit").click(function() {
                  var adminUsername = $("#admin-username").val();
                  var adminPassword = $("#admin-password").val();

                  var admin = {
                      "username":adminUsername,
                      "password":adminPassword
                  };
                  ajaxPost("auth/admin/login/", JSON.stringify(admin), null, setLoggedUser, "register.jsp");
              });*/

              $("button#professor-submit").click(function() {
                  var professorUsername = $("#professor-username").val();
                  var professorPassword = $("#professor-password").val();

                  var professor = {
                      "username":professorUsername,
                      "password":professorPassword,
                      "type":"Professor"
                  };
                  ajaxPost("auth/professor/login/", JSON.stringify(professor), null, setLoggedUser, "examinations.jsp");
              });

              $("button#professor-register").click(function() {
                  window.location = "register.jsp";
              });

/*                $("button#student-submit").click(function() {
                  var studentUsername = $("#student-username").val();
                  var studentPassword = $("#student-password").val();

                  var student = {
                      "username":studentUsername,
                      "password":studentPassword
                  };
                  ajaxPost("auth/student/login/", JSON.stringify(student), null, setLoggedUser, "index.jsp");
              });*/
          });
      </script>
   </body>
</html>