<!DOCTYPE html>
<html>
<head>
   <title>Virtual Clasroom - Examinations</title>
   <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
   <!-- Custom CSS -->
   <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
   <!-- Custom Fonts -->
   <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <!-- Morris Charts CSS -->
   <link href="startbootstrap-sb-admin-gh-pages/css/plugins/morris.css" rel="stylesheet">
   <!-- Custom Fonts -->
   <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <!-- Brand and toggle get grouped for better mobile display -->
   <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.jsp">Home</a>
   </div>
   <!-- Top Menu Items -->
   <ul class="nav navbar-right top-nav">
      <li class="dropdown">
         <a href="" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <text id="loggedName"></text>
            <b class="caret"></b>
         </a>
         <ul class="dropdown-menu">
            <li>
               <a class="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
            </li>
         </ul>
      </li>
   </ul>
</nav>
<div id="page-wrapper">
   <div class="container-fluid">
      <div class="form-group">
         <label>Examination Name:</label>
         <label id="examination-name"></label>
      </div>
      <div class="form-group">
         <label>Examination Type:</label>
         <label id="examination-type"></label>
      </div>
      <div class="form-group">
         <label>Examination Instance Name:</label>
         <input id="examination-instance-name" class="form-control" placeholder="Instance">
         <button id="create-instance" type="submit" class="btn btn-primary">Create Instance</button>
      </div>
      <div id="questions">
      </div>
      <div>
         <button id="delete-template" type="submit" class="btn btn-primary">Delete Template</button>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
<script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
<script src="js/ajax.js"></script>
<script src="js/authentication.js"></script>
<script type="application/javascript">
    var examination;

    $(document).ready(function() {

        var hosts = ['Professor', 'Assistant'];
        redirect(hosts);

        fillNavBar($('#loggedName'));
        var examinationId = {
            "id":getUrlVars()['id']
        };

        ajaxGet('examination/get', examinationId, null, renderExamination);
        if(getUrlVars()['instance']) {
            disableQuestions();
        }

        $('canvas').click(function(e) {
            var offset = $(this).offset();
            var X = e.pageX - offset.left;
            var Y = e.pageY - offset.top;

            drawCircle(this, X, Y);
        });

        function drawCircle(canvas, X, Y) {
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = "#0000FF";
            context.beginPath();
            context.fillRect(X, Y, 10, 10);
            context.stroke();
            context.fill();
        }

        $('body').on('click', 'a.logout', function(e) {
            logout();
            e.preventDefault();
        });
        
        $('body').on('click', 'button.publish-results', function(e) {
        	var questionInstance = {
                "instance" : localStorage.getItem("instanceName"),
                "questionId" : $(this).parent().prev().attr("id")
            };

        	ajaxGet('examination/question/publish', questionInstance);

        	$(this).attr('disabled','disabled');
        	
        });

        $('body').on('click', 'button#delete-template', function(e) {
            var questionInstance = {
                "id" : getUrlVars()['id'],
            };

            ajaxGet("examination/delete", questionInstance, null, null, "examinations.jsp");
        });
        
        $('body').on('click', 'button#create-instance', function() {

            var instanceName = $('#examination-instance-name').val();

            localStorage.setItem('instanceName', instanceName);
            localStorage.setItem('courseId', getUrlVars()['id']);
            if(instanceName !== '') {
                var instance = {
                    "id" : getUrlVars()['id'],
                    "instance" : instanceName,
                    "professorId" : JSON.parse(localStorage.getItem("loggedUserName"))[0].id
                };

                ajaxGet('examination/activate/', instance, null, redirectExamInstance);
            } else {
                $("#examination-instance-name").attr("placeholder", "Please insert instance name");
            }
        });

        $('body').on('click', 'button.enable-question', function() {

            var questionInstance = {
                "examinationId" : getUrlVars()['id'],
                "instance" : localStorage.getItem("instanceName"),
                "questionId" : $(this).parent().prev().attr("id"),
                "duration" : $(this).prev().val(),
                "seeResults" : $(this).parent().find('.see-results-asap').prop('checked')
            };

            ajaxGet("examination/question/activate", questionInstance, null, disableQuestions);
        });

        $('body').on('click', 'button.view-results', function() {

            var courseId = localStorage.getItem('courseId');
            var instanceName = localStorage.getItem('instanceName');

            window.location = 'view_results.jsp?id=' + courseId + '&instance=' + instanceName + '&question=' + $(this).parent().prev().attr('id');
        });

        $('body').on('click', 'button.show-correct-results', function() {
            var questionDiv = $(this).parent().prev();

            if(questionDiv.hasClass("Radio") || questionDiv.hasClass("Checkbox")) {
                var isChecked = questionDiv.find(".checked").is(":checked");
                if(isChecked) {
                    questionDiv.find(".checked").prop("checked", !isChecked);
                    $(this).html("Show Correct Results");
                } else {
                    questionDiv.find(".checked").prop("checked", !isChecked);
                    $(this).html("Hide Correct Results");
                }
            } else if(questionDiv.hasClass("Fill")) {
                var answer = questionDiv.find("textarea").attr('id');
                if(questionDiv.find("textarea").val() == "") {
                    questionDiv.find("textarea").val(answer);
                    $(this).html("Hide Correct Results");
                } else {
                    questionDiv.find("textarea").val("");
                    $(this).html("Show Correct Results");
                }
            } else if(questionDiv.hasClass("Picture")) {

            }
        });
    });

    function disableQuestions() {

        var instance = {
            "id": getUrlVars()['id'],
            "instance": getUrlVars()['instance']
        };
        ajaxGet("examination/instance/get", instance, null, doDisableQuestions);
    }

    function doDisableQuestions(data) {
        for(var i = 0; i < data.questions.length; i ++) {
            $("#" + data.questions[i].questionId).next().find("button.enable-question").attr('disabled','disabled');
            $("#" + data.questions[i].questionId).next().find("button.enable-question").next().prop('disabled', false);

            $("#" + data.questions[i].questionId).next().find("button.publish-results").prop('disabled', data.questions[i].seeResults);
        }
    }

    function redirectExamInstance() {

        var courseId = localStorage.getItem('courseId');
        var instanceName = localStorage.getItem('instanceName');
        window.location = 'view_examination.jsp?id=' + courseId + '&instance=' + instanceName;
    }

    function renderExamination(data) {
        $('#examination-name').html(data.name);
        $('#examination-type').html(data.type);
        $('#examination-instance-name').val(getUrlVars()['instance']);

        for(var i = 0; i < data.questions.length; i++) {
            var question = data.questions[i];

            var answers = '';
            for(var j = 0; j < question.answers.length; j++) {
                var answer = question.answers[j];

                if(question.type === 'Checkbox') {
                    var checked = '';
                    if(answer.isCorrect) {
						checked = 'checked';
                    }
                    answers += '<div class="checkbox"> <label id="' + answer.id + '"> <input type="checkbox" class="'+ checked +'" disabled value="">' + answer.answer + '</label> </div>';
                } else if(question.type === 'Radio') {
                	var checked = '';
                    if(answer.isCorrect) {
						checked = 'checked';
                    }
                    answers += '<div class="radio"> <label id="' + answer.id + '"> <input type="radio" class="'+ checked +'" disabled name="optionsRadios">' + answer.answer + '</label> </div>';
                } else if(question.type == "Fill") {
                    answers += '<textarea class="form-control" disabled rows="3" placeholder="Your Answer" id="' + answer.answer + '"></textarea>';
                }
            }

            if(question.type === 'Picture') {
                answers += '<canvas id="canvas' + question.id + '"></canvas>';

                answers += '<img id="picture' + question.id + '" src="../../' + question.pictureURL + '" alt="Image preview...">';
            }



            $('#questions').append('<div id="' + question.id + '" class="form-group ' + question.type + '"> <h1 class="name">' + question.question + '</h1>' + answers + ' <input id="' + question.id + '" class="hidden" type="hidden"></input> </div> </div>');

            $('#questions').append('<div class="form-group"><div><input class="see-results-asap" type="checkbox" value=""><span>Should students see results immediatelly?</span></div> Duration: <input class="form-control duration" placeholder="Duration" value="' + question.duration + '"> <button type="submit" class="btn btn-primary enable-question">Enable Question</button> <button type="submit" disabled class="btn btn-primary view-results">View Results</button> <button type="submit" class="btn btn-primary publish-results">Publish Results</button> <button type="submit" class="btn btn-primary show-correct-results">Show Correct Results</button></div><hr>');

            if(question.type === 'Picture') {
                var canvas = $("#canvas" + question.id);
                canvas.css("background-image", "url(../../" + question.pictureURL + ")");
                var picture = $("#picture" + question.id);
                var height = picture.height();
                var width = picture.width();

                canvas.height(150);
                canvas.width(300);

                picture.remove();
            }
        }
    }

    function loadImageInCanvas(canvas) {
        canvas.css("background-image", "url(images/whitetexturepaper.jpg)")
    }

    function renderPicture(data) {

    }

    function previewFile(){
        var preview = document.querySelector('img'); //selects the query named img
        var file    = document.querySelector('input[type=file]').files[0]; //sames as here
        var reader  = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file); //reads the data as a URL
        } else {
            preview.src = "";
        }
    }

    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
</script>
</body>
</html>