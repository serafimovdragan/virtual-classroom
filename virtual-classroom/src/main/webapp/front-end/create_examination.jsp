<!DOCTYPE html>
<html>
   <head>
      <title>Virtual Clasroom - Examinations</title>
      <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
      <!-- Custom Fonts -->
      <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   </head>
   <body>
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp">Home</a>
         </div>
         <!-- Top Menu Items -->
         <ul class="nav navbar-right top-nav">
            <li class="dropdown">
               <a href="" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user"></i>
                  <text id="loggedName"></text>
                  <b class="caret"></b>
               </a>
               <ul class="dropdown-menu">
                  <li>
                     <a class="logout" href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                  </li>
               </ul>
            </li>
         </ul>
      </nav>
      <div id="page-wrapper">
         <div class="container-fluid">
            <div class="form-group">
               <label>Examination Name</label>
               <input id="examination-name" class="form-control">
            </div>
            <div class="form-group">
               <label for="examination-type">Examination Type</label>
               <select id="examination-type" class="form-control">
                  <option>Test</option>
                  <option>Survey</option>
               </select>
                <span>* Please also insert the correct answers.</span>
            </div>
            <div id="questions">

            </div>
            <div class="form-group">
               <button id="add-checkbox" type="submit" class="btn btn-primary">Add Multi Answer Question</button>
               <button id="add-radio" type="submit" class="btn btn-primary">Add Single Answer Question</button>
               <button id="add-text" type="submit" class="btn btn-primary">Add Text Question</button>
               <!--<button id="add-picture" type="submit" class="btn btn-primary">Add Picture Question</button>-->
            </div>
            <hr>
            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
         </div>
      </div>

      <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
      <!-- Morris Charts JavaScript -->
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
      <script src="js/ajax.js"></script>
      <script src="js/authentication.js"></script>
      <script type="application/javascript">
            var pictures = {};
            var questionNumber = 1;

          $(document).ready(function() {

              var hosts = ['Professor', 'Assistant'];
              redirect(hosts);

              var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));
              var loggedUserName = JSON.parse(localStorage.getItem('loggedUserName'))[0];

              fillNavBar($('#loggedName'));

              $('body').on('click', 'canvas', function(e) {
                  var offset = $(this).offset();
                  var X = e.pageX - offset.left;
                  var Y = e.pageY - offset.top;

                  drawCircle(this, X, Y);
              });

              function drawCircle(canvas, X, Y) {
                  var context = canvas.getContext("2d");
                  context.clearRect(0, 0, canvas.width, canvas.height);
                  context.fillStyle = "#0000FF";
                  context.beginPath();
                  context.fillRect(X, Y, 10, 10);
                  context.stroke();
                  context.fill();
              }

              $('body').on('click', 'a.logout', function(e) {
                  logout();
                  e.preventDefault();
              });

              $('body').on('click', '#add-checkbox', function() {
                  $("#questions").append('<div class="form-group multi-answer"> <h1>' + questionNumber + '</h1> <div class="form-group"> <textarea class="form-control question-text" rows="3" placeholder="Multi Answer Question"></textarea> </div> <div id="' + questionNumber + '" class="answers"> </div> <button type="submit" class="btn btn-primary add-checkbox-answer">Add Answer</button><button type="submit" class="btn btn-primary delete-question">Delete Question</button> <div class="duration-div"> <div>Question duration in seconds</div> <div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="10"> 10 </label> </div><div class="radio"> <label> <input class="duration" checked type="radio" name="question-duration'+ questionNumber +'" value="30"> 30 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="60"> 60 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value=""><input class="form-control duration-text"></label> </div> </div> <hr/> </div>');
                  questionNumber++;
              });

              $('body').on('click', '#add-radio', function() {
                  $("#questions").append('<div class="form-group single-answer"> <h1>' + questionNumber + '</h1> <div class="form-group"> <textarea class="form-control question-text" rows="3" placeholder="Single Answer Question"></textarea> </div> <div id="' + questionNumber + '"  class="answers"> </div> <button type="submit" class="btn btn-primary add-radio-answer">Add Answer</button> <button type="submit" class="btn btn-primary delete-question">Delete Question</button> <div class="duration-div"> <div>Question duration in seconds</div> <div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="10"> 10 </label> </div><div class="radio"> <label> <input class="duration" checked type="radio" name="question-duration'+ questionNumber +'" value="30"> 30 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="60"> 60 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value=""><input class="form-control duration-text"></label> </div> </div> <hr/> </div>');
                  questionNumber++;
              });

              $('body').on('click', '#add-text', function() {
                  $("#questions").append('<div class="form-group text-answer"> <h1>' + questionNumber + '</h1> <div class="form-group"> <textarea class="form-control question-text" rows="3" placeholder="Text Question"></textarea> <div id="' + questionNumber + '" class="answers"><textarea class="form-control answer" rows="3" placeholder="Text Answer"></textarea></div> </div> <button type="submit" class="btn btn-primary delete-question">Delete Question</button> <div class="duration-div"> <div>Question duration in seconds</div> <div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="10"> 10 </label> </div><div class="radio"> <label> <input class="duration" checked type="radio" name="question-duration'+ questionNumber +'" value="30"> 30 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="60"> 60 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value=""><input class="form-control duration-text"></label> </div> </div> <hr/> </div>');
                  questionNumber++;
              });

              $('body').on('click', '#add-picture', function() {
                  $("#questions").append('<div class="form-group picture-answer"> <h1>' + questionNumber + '</h1> <div class="form-group"> <textarea class="form-control question-text" rows="3" placeholder="Picture Question"></textarea> </div> <div class="form-group"> <label>File input</label> <form id="file_upload_form" enctype="multipart/form-data"><input id="attachFile' + questionNumber + '" class="form-control" type="file" name="file" onchange=previewFile(this) size="50"/></form><br>\n <img id="picture' + questionNumber + '" src="" alt="Image preview..."><canvas id="canvas' + questionNumber + '" height="0" width="0"></canvas> </div> <div id="' + questionNumber + '" class="answers"> </div> <button type="submit" class="btn btn-primary delete-question">Delete Question</button> <div class="duration-div"> <div>Question duration in seconds</div> <div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="10"> 10 </label> </div><div class="radio"> <label> <input class="duration" checked type="radio" name="question-duration'+ questionNumber +'" value="30"> 30 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value="60"> 60 </label> </div><div class="radio"> <label> <input class="duration" type="radio" name="question-duration'+ questionNumber +'" value=""><input class="form-control duration-text"></label> </div> </div> <hr/> </div>');
                  questionNumber++;
              });

              $('body').on('click', '.add-checkbox-answer', function() {
                  $(this).prev().append('<div class="checkbox"> <label> <input class="correct" type="checkbox" value=""><input class="form-control answer"></label> </div>');
              });

              $('body').on('click', '.add-radio-answer', function() {
                  console.log($(this).prev());
                  $(this).prev().append('<div class="radio"> <label> <input class="correct" type="radio" name="optionsRadios'+ $(this).prev().attr('id') +'" id="optionsRadios2" value=""><input class="form-control answer"></label> </div>');
              });

              $('body').on('click', '.delete-question', function() {
                  $(this).parent().remove();
                  questionNumber--;
              });

              $('body').on('click', '#submit-btn', function() {
                 e.preventDefault();
              });


              $('body').on('click', '#submit', function() {
                  var examination = {};

                  examination.courseId = getUrlVars()["course_id"];
                  examination.name = $('#examination-name').val();
                  examination.type = $('#examination-type').val();

                  examination.questions = [];

                  $('#questions').children().each(function () {
                     var question = {};

                     question.question = $(this).find('textarea.question-text').val();

                     question.pictureURL = pictures[$(this).find("h1").html()];

                      if($(this).hasClass('multi-answer')) {
                          question.type = 'Checkbox';
                      } else if($(this).hasClass('single-answer')) {
                          question.type = 'Radio';
                      } else if($(this).hasClass('text-answer')) {
                          question.type = 'Fill';
                      } else if($(this).hasClass('picture-answer')) {
                          question.type = 'Picture';
                      }

                      question.answers = [];

                      $(this).find('.answers').children().each(function() {
                          var answer = {};

                          answer.answer = $(this).find('.answer').val();
                          if(answer.answer == null) { // i suck
                              answer.answer = $(this).val();
                          }
                          if(examination.type == "Test" && (question.type == "Radio" || question.type == "Checkbox")) {
                              answer.isCorrect = $(this).find('.correct').is(':checked');
                          } else if(examination.type == "Test" && question.type == "Fill") {
                              answer.isCorrect = true;
                          } else {
							  answer.isCorrect = false;
                          }

                          question.answers.push(answer);
                      });

                      question.duration = $('input[name=question-duration'+ $(this).find('.answers').attr('id') +']:checked').val();

                      if(question.duration == "") {
                          question.duration = $(this).find('.duration-text').val();
                      }
                      
                      examination.questions.push(question);
                  });

                  ajaxPost("examination/insert/", JSON.stringify(examination), null, examinationCreated);
              });
          });

          function previewFile(object){
              var parent = object.parentElement.parentElement;
              var preview = parent.querySelector('img'); //selects the query named img
              var file    = parent.querySelector('input[type=file]').files[0]; //sames as here
              var reader  = new FileReader();

              reader.onloadend = function () {
                  preview.src = reader.result;
              }

              if (file) {
                  reader.readAsDataURL(file); //reads the data as a URL
              } else {
                  preview.src = "";
              }

              var formData = new FormData();

              formData.append("picture", file);

              ajaxPostUpload("examination/picture/upload/", formData, null, successfulUpload);
          }

          function successfulUpload(data) {
              pictures[questionNumber - 1] = data;

              var picture = $("#picture" + (questionNumber - 1));
              var canvas = $("#canvas" + (questionNumber - 1));

              console.log(picture.src);
              canvas.css("background-image", "url(../../" + data + ")");
              canvas.height(150);
              canvas.width(300);

              picture.remove();
          }

          function examinationCreated() {
              window.location = "examinations.jsp";
          }

          function getUrlVars()
          {
              var vars = [], hash;
              var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
              for(var i = 0; i < hashes.length; i++)
              {
                  hash = hashes[i].split('=');
                  vars.push(hash[0]);
                  vars[hash[0]] = hash[1];
              }
              return vars;
          }
      </script>
   </body>
</html>