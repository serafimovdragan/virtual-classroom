<!DOCTYPE html>
<html>
   <head>
      <title>Virtual Clasroom - Login</title>
      <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
      <!-- Custom Fonts -->
      <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   </head>
   <body>
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp">SB Admin</a>
         </div>
         <!-- Top Menu Items -->
      <ul class="nav navbar-right top-nav">
         <li class="dropdown">
            <a href="login.jsp" class="dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-user"></i>
               <text id="loggedName"></text>
               <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
               <li>
                  <a class="logout" href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
               </li>
            </ul>
         </li>
      </ul>
      </nav>
      <div id="page-wrapper">
         <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
               <div class="col-lg-4"></div>
               <div class="col-lg-4">
                  <h1 class="page-header">
                     Register
                  </h1>
               </div>
               <div class="col-lg-4"></div>
            </div>
            <!-- /.row -->
            <div class="form-group col-lg-4">

            </div>
            <div class="form-group col-lg-4">
               <label>Professor Register</label>
               <input id="professor-username" class="form-control" placeholder="Username">
               <input id="professor-password" type="password" class="form-control" placeholder="Password">
               <input id="professor-repeat-password" type="password" class="form-control" placeholder="Repeat Password">
               <input id="professor-name" class="form-control" placeholder="Name">
               <input id="professor-surname" class="form-control" placeholder="Surname">
               <hr>
               <button id="professor-register" type="submit" class="btn btn-primary">Professor Register</button>
            </div>
            <div class="form-group col-lg-4">

            </div>
         </div>
         <!-- /.container-fluid -->
      </div>
   <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
   <!-- Bootstrap Core JavaScript -->
   <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
   <!-- Morris Charts JavaScript -->
   <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
   <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
   <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
   <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
   <script src="js/ajax.js"></script>
   <script src="js/authentication.js"></script>
      <script type="text/javascript">

       var username = "";
       var password = "";

       $(document).ready(function() {

           var hosts = ['None'];
           redirect(hosts);

           var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

           if(loggedUser) {
               fillNavBar($('#loggedName'));
           } else {
               $(".top-nav").html('<li class="dropdown login-redirect"> <a href="login.jsp" data-toggle="dropdown"><i class="fa fa-user"></i> Login <b class="caret"></b> </li>');
           }

           $('body').on('click', 'a.logout', function(e) {
               logout();
               e.preventDefault();
           });

           $('body').on('click', 'a.logout', function(e) {
               logout();
               e.preventDefault();
           });

           $("button#professor-register").click(function() {
               var professorUsername = $("#professor-username").val();
               var professorPassword = $("#professor-password").val()
               var professorRepeatPassword = $("#professor-repeat-password").val();
               var professorName = $("#professor-name").val();
               var professorSurname = $("#professor-surname").val();

               var professor = {
                   "username":professorUsername,
                   "password":professorPassword,
                   "repeatPassword":professorRepeatPassword,
                   "type":"Professor",
                   "name":professorName,
                   "surname":professorSurname
               };

               username = professorUsername;
               password = professorPassword;
               ajaxPost("admin/professor/insert", JSON.stringify(professor), null, professorLogin, null, errorFunction);


           });

           $(".login-redirect").click(function() {
               window.location = "login.jsp";
           });

       });

       function professorLogin() {
           if(username == "" || password == "") {
               return;
           }
           var professorLogin = {
               "username":username,
               "password":password,
               "type":"Professor"
           };
           ajaxPost("auth/professor/login/", JSON.stringify(professorLogin), null, setLoggedUser, "examinations.jsp");
       }

       function errorFunction(data) {
           username = "";
           password = "";
           console.log(data);
       }
      </script>
   </body>
</html>