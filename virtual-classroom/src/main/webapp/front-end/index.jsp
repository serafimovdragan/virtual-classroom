<html>
<head>
   <title>Virtual Clasroom - Index</title>
   <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
   <!-- Custom CSS -->
   <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
   <!-- Custom Fonts -->
   <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <!-- Morris Charts CSS -->
   <link href="startbootstrap-sb-admin-gh-pages/css/plugins/morris.css" rel="stylesheet">
   <!-- Custom Fonts -->
   <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <!-- Brand and toggle get grouped for better mobile display -->
   <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.jsp">Home</a>
   </div>
   <!-- Top Menu Items -->
   <ul class="nav navbar-right top-nav">
      <li class="dropdown">
         <a href="" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <text id="loggedName"></text>
            <b class="caret"></b>
         </a>
         <ul class="dropdown-menu">
            <li> <a class="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a> </li>
         </ul>
      </li>
   </ul>
</nav>
<div id="page-wrapper">
   <div class="container-fluid">
      <div class="col-lg-4"></div>
      <div class="col-lg-4">
         <form>
            <div class="form-group">
               <div class="col-lg-6">
                  <input id="instance-name" class="form-control" placeholder="Find Exam Or Survey">
               </div>
               <div class="col-lg-6">
                  <button id="find-exam" type="submit" class="btn btn-primary">Find</button>
               </div>
            </div>
         </form>
      </div>
      <div class="col-lg-4"></div>
   </div>
   <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
   <!-- Bootstrap Core JavaScript -->
   <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
   <!-- Morris Charts JavaScript -->
   <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
   <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
   <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
   <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
   <script src="js/ajax.js"></script>
   <script src="js/authentication.js"></script>
   <script type="text/javascript">

       $(document).ready(function() {

           var examination = localStorage.getItem("examination");

           if(examination) {
               window.location = "take_examination.jsp?examination=" + examination;
           }

           var hosts = ['Student', 'None'];
           redirect(hosts);

           var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

           if(loggedUser) {
               fillNavBar($('#loggedName'));
           } else {
               $(".top-nav").html('<li class="dropdown login-redirect"> <a href="login.jsp" data-toggle="dropdown"><i class="fa fa-user"></i> Login <b class="caret"></b> </li>');
           }

           $('body').on('click', 'a.logout', function(e) {
               logout();
               e.preventDefault();
           });

           $('body').on('click', '#find-exam', function() {

               var name = {
                   "name": $("#instance-name").val()
               };

               ajaxGet('student/examination', name, null, redirectExam, "take_examination.jsp");
           });

           $('body').on('click', '.login-redirect', function() {
                window.location = "login.jsp";
           });

       });

       function redirectExam() {
           window.location = "take_examination?examination=" + window.localStorage.getItem("examination");
           window.localStorage.setItem("examination", $("#instance-name").val());
       }
   </script>
</body>
</html>