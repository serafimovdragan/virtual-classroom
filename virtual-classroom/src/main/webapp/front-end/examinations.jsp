<!DOCTYPE html>
<html>
   <head>
      <title>Virtual Clasroom - Examinations</title>
      <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">
      <!-- Custom Fonts -->
      <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   </head>
   <body>
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp">Home</a>
         </div>
         <!-- Top Menu Items -->
         <ul class="nav navbar-right top-nav">
            <li class="dropdown">
               <a href="" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user"></i>
                  <text id="loggedName"></text>
                  <b class="caret"></b>
               </a>
               <ul class="dropdown-menu">
                  <li>
                     <a class="logout" href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                  </li>
               </ul>
            </li>
         </ul>
      </nav>
      <div id="page-wrapper">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-4">
                  <div class="list-group courses">
                  </div>
                  <div id="add-course">
                     <input id="insert-course-name" class="form-control" placeholder="Course Name">
                     <button id="add-course" type="submit" class="btn btn-primary">Add Course</button>
                  </div>
               </div>
               <!-- /.col-sm-4 -->
               <div class="col-sm-4">
                  <div class="list-group examinations">
                  </div>
               </div>
               <button type="submit" class="btn btn-primary create-examination" disabled>Create Examination</button>
            </div>
         </div>
      </div>
      <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>
      <!-- Morris Charts JavaScript -->
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>
      <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>
      <script src="js/ajax.js"></script>
      <script src="js/authentication.js"></script>
      <script type="application/javascript">
          $(document).ready(function() {

              var hosts = ['Professor', 'Assistant'];
              redirect(hosts);

              var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));
              var loggedUserName = JSON.parse(localStorage.getItem('loggedUserName'))[0];

              fillNavBar($('#loggedName'));

              $('body').on('click', 'a.logout', function(e) {
                  logout();
                  e.preventDefault();
              });

              var clickedCourse = 0;
              var professorId = {
                  "professorId":loggedUserName.id
              };
              ajaxGet("professor/courses", professorId, window.localStorage.getItem("token"), fillCourses);

              $('body').on('click', 'a.course', function() {
                  $(".course").removeClass("active");
                  $(this).addClass("active");
                  clickedCourse = $(this).attr("id");
                  var courseId = {
                      "courseId":$(this).attr("id")
                  };
                  ajaxGet("professor/courses/examinations", courseId, window.localStorage.getItem("token"), fillExaminations);
                  $("button.create-examination").prop("disabled", false);
              });

              $('body').on('click', 'a.examination', function() {
                  $(".examination").removeClass("active");
                  $(this).addClass("active");
                  var examinationId = {
                     "id":$(this).attr("id")
                  };
                  window.location = "view_examination.jsp?id=" + $(this).attr("id");
                  //ajaxGet("examination/get", examinationId, window.localStorage.getItem("token"), null, "view_examination.jsp");
              });

              $('body').on('click', 'button.create-examination', function() {
                  window.location = "create_examination.jsp?course_id=" + clickedCourse;
              })

              $('body').on('click', 'button#add-course', function() {
                  var course = {
                      "name":$("#insert-course-name").val(),
                      "professorId":loggedUserName.id,
                      "credits":"6"
                  };

                  ajaxPost("admin/course/insert", JSON.stringify(course), null, successAddedCourse, null, errorAddedCourse);
              });
          });

          function successAddedCourse() {
              window.location = "examinations.jsp";
          }

          function errorAddedCourse(data) {
              alert(data.msg);
          }

          function fillCourses(data) {
              for(var i = 0; i < data.length; i ++) {
                  $(".courses").append('<a id="' + data[i].id + '" href="#" class="list-group-item course">' + data[i].name + '</a>');
              }
          }

          function fillExaminations(data) {
              $(".examinations").empty();
              for(var i = 0; i < data.length; i ++) {
                  $(".examinations").append('<a id="' + data[i].id + '" href="#" class="list-group-item examination">' + data[i].name + '</a>');
              }
          }
      </script>
   </body>
</html>