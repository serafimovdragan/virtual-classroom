-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: virtual_classroom
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_sessions`
--

DROP TABLE IF EXISTS `admin_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `session` varchar(64) DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `admin_sessions_fk` (`admin_id`),
  CONSTRAINT `admin_sessions_fk` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_sessions`
--

LOCK TABLES `admin_sessions` WRITE;
/*!40000 ALTER TABLE `admin_sessions` DISABLE KEYS */;
INSERT INTO `admin_sessions` VALUES (1,2,'Admin:ec1a52a2-8823-416a-aed8-5ffb84e40412','2017-12-06 18:22:04','2017-12-06 19:22:04'),(2,2,'Admin:79ac22c6-cfd4-4d0f-8ca9-2b7ff7c020d9','2017-12-28 09:44:05','2017-12-28 10:44:05');
/*!40000 ALTER TABLE `admin_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(64) DEFAULT NULL,
  `salt` varchar(64) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `surname` varchar(40) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_activity` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (2,'test','efa8821c6c03899893a5dcfd0b0f46df66a4b143478f0ae8a43170f6268caa8a','202da44c084648445095c7698014e097e4b7562c91d914db8a0725903bda5798','test1','test','2017-03-07 15:14:53','2017-03-07 15:14:53');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) unsigned DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `is_correct` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `fk_question_id_idx` (`question_id`),
  CONSTRAINT `FK3erw1a3t0r78st8ty27x6v3g1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  CONSTRAINT `fk_question_id` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,18,'ne','\0'),(2,18,'da',''),(3,21,'treto','\0'),(4,21,'prvo',''),(5,21,'vtoro','\0');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers_submitted`
--

DROP TABLE IF EXISTS `answers_submitted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers_submitted` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `examination_id` int(11) unsigned DEFAULT NULL,
  `instance` varchar(250) DEFAULT NULL,
  `question_id` int(11) unsigned DEFAULT NULL,
  `question_type` tinyint(4) DEFAULT NULL,
  `answer_id` int(11) unsigned DEFAULT NULL,
  `answer` varchar(250) DEFAULT NULL,
  `points` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index2` (`student_id`,`instance`,`question_id`,`answer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers_submitted`
--

LOCK TABLES `answers_submitted` WRITE;
/*!40000 ALTER TABLE `answers_submitted` DISABLE KEYS */;
INSERT INTO `answers_submitted` VALUES (1,19,11,'tes',21,0,5,NULL,NULL),(3,19,11,'tes',20,2,NULL,'сфасдф',NULL),(5,19,11,'tes',18,1,2,NULL,NULL),(6,19,11,'qwer',21,0,5,NULL,NULL),(7,19,11,'qwer',18,1,2,NULL,NULL),(8,19,11,'qwerrqqq',21,0,3,NULL,NULL),(9,19,11,'qwerrqqq',20,2,NULL,'fdfaf',NULL),(10,19,11,'qwerrqqq',19,3,NULL,NULL,NULL),(11,19,11,'qqq',18,1,1,NULL,NULL),(12,19,11,'qqq',21,0,3,NULL,NULL),(13,19,11,'qqq',20,2,NULL,'fffs',NULL),(14,19,11,'qqqq',21,0,4,NULL,NULL),(15,19,11,'qqqq',18,1,1,NULL,NULL),(16,19,11,'qqqqqq',21,0,5,NULL,NULL),(17,19,11,'qqqqqqq',21,0,4,NULL,NULL),(18,19,11,'qqqqqqqq',21,0,4,NULL,NULL),(19,19,11,'qqqqqqqqq',21,0,3,NULL,NULL),(20,19,11,'a',21,0,5,NULL,NULL),(21,19,11,'aa',21,0,3,NULL,NULL),(24,19,11,'aaaaa',21,0,4,NULL,NULL),(25,19,11,'aaaaa',21,0,5,NULL,NULL),(26,19,11,'aaaaa',18,1,1,NULL,NULL),(27,19,11,'aaaaa',20,2,NULL,'shoz',NULL);
/*!40000 ALTER TABLE `answers_submitted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `credits` int(10) unsigned NOT NULL,
  `professor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_professor_id_idx` (`professor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (5,'Test Course',5,18);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examinations`
--

DROP TABLE IF EXISTS `examinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examinations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` smallint(5) unsigned NOT NULL,
  `professor_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `creation_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `course_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_creator_id_idx` (`professor_id`),
  KEY `idx_name` (`name`),
  KEY `fk_course_id_idx` (`course_id`),
  CONSTRAINT `FKh7co4gaiwgyp4m81s7q2iryur` FOREIGN KEY (`professor_id`) REFERENCES `professors` (`id`),
  CONSTRAINT `fk_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professor_id` FOREIGN KEY (`professor_id`) REFERENCES `professors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examinations`
--

LOCK TABLES `examinations` WRITE;
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
INSERT INTO `examinations` VALUES (11,0,NULL,'Test Examination','2017-12-06 19:25:41',5);
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examinations_instances`
--

DROP TABLE IF EXISTS `examinations_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examinations_instances` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `examination_id` int(11) unsigned NOT NULL,
  `professor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `instance` (`examination_id`,`name`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_professor_idx` (`professor_id`),
  CONSTRAINT `FK7pik9bj7u8a1mjkixmhcqu5go` FOREIGN KEY (`examination_id`) REFERENCES `examinations` (`id`),
  CONSTRAINT `FKsxtf3a633oqb901fk3h95lpmg` FOREIGN KEY (`professor_id`) REFERENCES `professors` (`id`),
  CONSTRAINT `fk_examination` FOREIGN KEY (`examination_id`) REFERENCES `examinations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_professors` FOREIGN KEY (`professor_id`) REFERENCES `professors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examinations_instances`
--

LOCK TABLES `examinations_instances` WRITE;
/*!40000 ALTER TABLE `examinations_instances` DISABLE KEYS */;
INSERT INTO `examinations_instances` VALUES (27,'first instance',11,18),(28,'tes',11,18),(29,'qwer',11,18),(31,'qwerr',11,18),(32,'qwerrr',11,18),(33,'wqeeeee',11,18),(34,'2231d',11,18),(35,'qwerrqqq',11,18),(36,'qqq',11,18),(37,'qqqq',11,18),(38,'qqqqq',11,18),(39,'qqqqqq',11,18),(40,'qqqqqqq',11,18),(41,'qqqqqqqq',11,18),(42,'qqqqqqqqq',11,18),(43,'qqqqqqqqqq',11,18),(44,'a',11,18),(45,'aa',11,18),(46,'aaaa',11,18),(47,'aaaaa',11,18);
/*!40000 ALTER TABLE `examinations_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor_sessions`
--

DROP TABLE IF EXISTS `professor_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `professor_id` int(11) NOT NULL,
  `session` varchar(64) DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `professor_sessions_fk` (`professor_id`),
  CONSTRAINT `professor_sessions_fk` FOREIGN KEY (`professor_id`) REFERENCES `professors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor_sessions`
--

LOCK TABLES `professor_sessions` WRITE;
/*!40000 ALTER TABLE `professor_sessions` DISABLE KEYS */;
INSERT INTO `professor_sessions` VALUES (1,18,'Professor:0620c747-6433-4c2c-9a56-708e2924ff2e','2017-12-06 18:21:38','2017-12-06 19:23:46'),(2,18,'Professor:1e7f0bf9-6445-4809-8dcd-bf7025823cfd','2017-12-28 09:19:24','2017-12-28 09:19:54'),(3,18,'Professor:ab8bd7e7-2ca5-4510-88c0-82708cc863d8','2017-12-28 09:20:07','2017-12-28 09:21:04'),(4,18,'Professor:caa09023-f627-4a05-a082-a9eb766d1490','2017-12-28 09:21:16','2017-12-28 09:21:43'),(5,18,'Professor:da1152bb-4820-40fc-99ec-b4e11788c7d0','2017-12-28 10:10:59','2017-12-28 11:10:59'),(6,18,'Professor:a565679d-9b27-4882-bf28-5dc80c150bac','2018-01-10 12:04:04','2018-01-10 13:04:04'),(7,18,'Professor:dd87da13-4fd0-4550-9adf-e0467af686f5','2018-01-16 09:39:04','2018-01-16 10:39:04');
/*!40000 ALTER TABLE `professor_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professors`
--

DROP TABLE IF EXISTS `professors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `surname` varchar(40) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(64) NOT NULL,
  `professor_type` tinyint(4) NOT NULL DEFAULT '0',
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `professors_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professors`
--

LOCK TABLES `professors` WRITE;
/*!40000 ALTER TABLE `professors` DISABLE KEYS */;
INSERT INTO `professors` VALUES (17,'profe','qq','tafffnt','e6000354b04572224c177efa2c093d7503fa4ae8b53b57c0fc078944fea2ccb8','caae7594c12ebf2a112ae7a76b0ca835e0a5780d080786c960056e547ea23f2d',1,'2017-02-11 08:42:04','2017-02-11 08:42:04'),(18,'test','test','test','d09e84981dca6443c59f2f2bc9633b6818317ade7acbcf61c7c13c480dbd290f','754d587674606f89f1816530a0197f896881c00193541019aa4967c8594b4a6b',0,'2017-03-07 13:24:12','2017-03-07 13:24:12'),(19,'1112','qq','tafffnt','fed98fe5a0c24a6499b684e6a76aa2f1fc5720e6dc38b09464d49da8e49ca2e0','1de7464df8553df2b57b7540df359bbca51c45cc48772cc052d1b5cdca9bc7b7',1,'2017-03-11 17:47:39','2017-03-11 17:47:39');
/*!40000 ALTER TABLE `professors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `properties` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties`
--

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `examination_id` int(11) unsigned NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `type` smallint(5) NOT NULL,
  `duration` int(11) unsigned DEFAULT NULL,
  `active` bit(1) DEFAULT b'0',
  `picture_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_examinations_id_idx` (`examination_id`),
  CONSTRAINT `FK1y24yn7v3jwjp7le455y5ki10` FOREIGN KEY (`examination_id`) REFERENCES `examinations` (`id`),
  CONSTRAINT `fk_examinations_id` FOREIGN KEY (`examination_id`) REFERENCES `examinations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (18,11,'dali?',1,NULL,'\0',NULL),(19,11,'Slika',3,NULL,'\0',NULL),(20,11,'Opis',2,NULL,'\0',NULL),(21,11,'Prvoto e tocno?',0,NULL,'\0',NULL);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions_instances`
--

DROP TABLE IF EXISTS `questions_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions_instances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examination_instance_id` int(11) unsigned NOT NULL,
  `question_id` int(11) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `activation_date` timestamp NULL DEFAULT NULL,
  `expiration_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_by_instance` (`question_id`,`examination_instance_id`),
  KEY `fk_examination_instance_idx` (`examination_instance_id`),
  CONSTRAINT `FKa712jcm5kvbt42cqfntpbsjxy` FOREIGN KEY (`examination_instance_id`) REFERENCES `examinations_instances` (`id`),
  CONSTRAINT `fk_examination_instance` FOREIGN KEY (`examination_instance_id`) REFERENCES `examinations_instances` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions_instances`
--

LOCK TABLES `questions_instances` WRITE;
/*!40000 ALTER TABLE `questions_instances` DISABLE KEYS */;
INSERT INTO `questions_instances` VALUES (1,28,21,10,'2017-12-28 10:11:48','2017-12-28 10:11:58'),(2,28,20,2,'2017-12-28 10:13:15','2017-12-28 10:13:17'),(3,28,18,10,'2017-12-28 10:28:19','2017-12-28 10:28:29'),(4,28,19,10,'2017-12-28 10:29:15','2017-12-28 10:29:25'),(5,29,21,30,'2018-01-10 12:05:49','2018-01-10 12:06:19'),(6,29,18,100,'2018-01-10 12:12:05','2018-01-10 12:13:45'),(7,29,20,30,'2018-01-10 12:46:48','2018-01-10 12:47:18'),(8,29,19,100,'2018-01-10 13:44:49','2018-01-10 13:46:29'),(9,31,18,500,'2018-01-10 13:57:57','2018-01-10 14:06:17'),(10,31,19,10,'2018-01-10 13:58:09','2018-01-10 13:58:19'),(11,31,21,1000,'2018-01-10 14:07:04','2018-01-10 14:23:44'),(12,31,20,20,'2018-01-10 14:07:09','2018-01-10 14:07:29'),(13,32,18,100,'2018-01-10 14:13:21','2018-01-10 14:15:01'),(14,32,21,10,'2018-01-10 14:13:21','2018-01-10 14:13:31'),(15,32,19,10,'2018-01-10 14:17:25','2018-01-10 14:17:35'),(16,32,20,30,'2018-01-10 14:17:25','2018-01-10 14:17:55'),(18,33,19,123,'2018-01-13 13:37:15','2018-01-13 13:39:18'),(19,33,20,22,'2018-01-13 13:39:28','2018-01-13 13:39:50'),(20,33,21,22,'2018-01-13 13:40:00','2018-01-13 13:40:22'),(21,33,18,22,'2018-01-13 13:40:31','2018-01-13 13:40:53'),(22,34,21,21,'2018-01-13 13:41:13','2018-01-13 13:41:34'),(23,34,19,55,'2018-01-13 13:43:14','2018-01-13 13:44:09'),(24,35,18,10,'2018-01-13 14:08:01','2018-01-13 14:08:11'),(25,35,21,100,'2018-01-13 14:39:51','2018-01-13 14:41:31'),(26,35,20,31,'2018-01-13 14:45:51','2018-01-13 14:46:22'),(27,35,19,11111,'2018-01-13 14:47:49','2018-01-13 17:53:00'),(28,36,18,100,'2018-01-14 13:44:25','2018-01-14 13:46:05'),(29,36,21,100,'2018-01-14 14:17:24','2018-01-14 14:19:04'),(30,36,20,100,'2018-01-14 14:18:59','2018-01-14 14:20:39'),(31,37,21,100,'2018-01-14 14:24:44','2018-01-14 14:26:24'),(32,37,18,10,'2018-01-14 14:29:44','2018-01-14 14:29:54'),(33,38,21,100,'2018-01-14 14:30:46','2018-01-14 14:32:26'),(34,39,21,1000,'2018-01-14 14:34:55','2018-01-14 14:51:35'),(35,40,21,1000,'2018-01-14 14:44:03','2018-01-14 15:00:43'),(36,41,21,1000,'2018-01-14 14:44:44','2018-01-14 15:01:24'),(37,42,21,1000,'2018-01-14 14:48:17','2018-01-14 15:04:57'),(38,43,21,1000,'2018-01-14 14:54:15','2018-01-14 15:10:55'),(39,44,21,1000,'2018-01-14 14:55:16','2018-01-14 15:11:56'),(40,45,21,1000,'2018-01-14 15:08:24','2018-01-14 15:25:04'),(41,46,21,1000,'2018-01-14 15:28:16','2018-01-14 15:44:56'),(42,47,21,1000,'2018-01-14 15:30:13','2018-01-14 15:46:53'),(43,47,18,100,'2018-01-14 15:30:45','2018-01-14 15:32:25'),(44,47,20,100,'2018-01-14 15:31:01','2018-01-14 15:32:41');
/*!40000 ALTER TABLE `questions_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_sessions`
--

DROP TABLE IF EXISTS `student_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `session` varchar(64) DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_sessions_fk` (`student_id`),
  CONSTRAINT `student_sessions_fk` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_sessions`
--

LOCK TABLES `student_sessions` WRITE;
/*!40000 ALTER TABLE `student_sessions` DISABLE KEYS */;
INSERT INTO `student_sessions` VALUES (1,19,'Student:ee6643a0-c2bb-436d-85ef-aed469ed8ff9','2017-12-06 18:26:43','2017-12-06 19:26:43',NULL),(2,19,'Student:78d00faa-34e7-4557-b0cc-4579ad504f5f','2017-12-28 09:19:00','2017-12-28 09:19:06',NULL),(3,19,'Student:3c403010-6188-4161-a7c9-fc59354061db','2017-12-28 09:21:47','2017-12-28 09:21:50',NULL),(4,19,'Student:dd442caa-2650-40e4-96ce-d3f7e7c3c107','2017-12-28 09:57:15','2017-12-28 10:57:15',NULL),(5,19,'Student:87130ed8-1ccc-457a-a109-d7fcf2debcf0','2018-01-10 12:05:04','2018-01-10 13:05:04',NULL),(6,19,'Student:6133fbbe-8b83-4f69-bc31-71bf8089a2d1','2018-01-13 14:39:32','2018-01-13 14:48:20',NULL),(7,19,'Student:7465472a-4268-4c81-bd2a-d1c29a2261e6','2018-01-14 13:44:03','2018-01-14 14:44:03',NULL),(8,19,'Student:3e0bed33-d4e7-4bd5-9cb2-32db6c919bbe','2018-01-14 14:55:04','2018-01-14 16:27:55',NULL);
/*!40000 ALTER TABLE `student_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(64) NOT NULL,
  `name` varchar(40) NOT NULL,
  `surname` varchar(40) NOT NULL,
  `credits` int(11) DEFAULT '0',
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `students_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (19,'test','0f76d00d835134c3c4b178fedaccb304827c39663fc9d09b3f2d2fd1f3b3f2b7','69dd969d073ee81a5878a57051e2f9e1aee7278a42ac442e57b54b561c0da2a4','test','test',4,'2017-03-08 12:26:26','2017-03-08 12:26:26');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_courses`
--

DROP TABLE IF EXISTS `students_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_courses` (
  `student_id` int(11) NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`student_id`,`course_id`),
  KEY `fk_courses_idx` (`course_id`),
  CONSTRAINT `fk_courses` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_students` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_courses`
--

LOCK TABLES `students_courses` WRITE;
/*!40000 ALTER TABLE `students_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_courses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-16 11:04:13
