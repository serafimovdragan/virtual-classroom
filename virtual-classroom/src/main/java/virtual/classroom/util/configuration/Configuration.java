/**
 * 
 */
package virtual.classroom.util.configuration;

import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * @author Dragan
 *
 */
@Stateless
public class Configuration {
	
	public static final String PERSISTENCE_CONTEXT_NAME = "virtual-classroom";
	
	private Properties properties = new Properties();

	@PersistenceContext(name = PERSISTENCE_CONTEXT_NAME)
	private EntityManager entityManager;
	
	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public void putProperty(String key, String value) {
		if(properties.containsKey(key)) {
			Query query = entityManager.createNativeQuery("update properties set value = ? where key = :key");
			query.setParameter(1, key);
			query.executeUpdate();
		} else {
			Query query = entityManager.createNativeQuery("insert into properties (`key`, value) values(?, ?)");
			query.setParameter(1, key);
			query.setParameter(2, value);
			query.executeUpdate();
		}
		
		properties.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getDatabaseProperties() {
		Query query = entityManager.createNativeQuery("select * from properties");
		return query.getResultList();
	}
}
