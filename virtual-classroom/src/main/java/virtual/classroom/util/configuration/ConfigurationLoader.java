/**
 * 
 */
package virtual.classroom.util.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * @author Dragan
 *
 */
@Startup
@Singleton
public class ConfigurationLoader {

	public static final String JBOSS_CONFIG = "jboss.server.config.dir";

	public static final String APPLICATION_PROEPERTIES_PATH = "/application.properties";

	@EJB
	private Configuration configuration;

	

	@PostConstruct
	public void loadProperties() {
		loadApplicationProperties();
		loadFileProperties();
		loadDatabaseProperties();
	}

	private void loadApplicationProperties() {
		for (ApplicationProperty property : ApplicationProperty.values()) {
			configuration.getProperties().put(property.getKey(), property.getDefaultValue());
		}
	}

	private void loadFileProperties() {
		String fileName = System.getProperty(JBOSS_CONFIG) + APPLICATION_PROEPERTIES_PATH;
		File file = new File(fileName);

		if (file.exists()) {
			InputStream is = null;
			try {
				is = new FileInputStream(file);
				configuration.getProperties().load(is);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void loadDatabaseProperties() {
		List<Object[]> result = configuration.getDatabaseProperties();
		for (Object[] row : result) {
			configuration.getProperties().put(row[0], row[1]);
		}
	}
}
