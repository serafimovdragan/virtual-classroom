package virtual.classroom.util.configuration;

public enum ApplicationProperty {

	TestMode("test.mode", "true");
	
	private String key;
	
	private String defaultValue;
	
	ApplicationProperty(String key, String defaultValue) {
		this.key = key;
		this.defaultValue = defaultValue;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
}
