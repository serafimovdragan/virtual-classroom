/**
 * 
 */
package virtual.classroom.util.errorhandling;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Dragan
 *
 */
public class AuthenticationErrorBean {

	@JsonProperty("code")
	private Integer code;
	
	@JsonProperty("msg")
	private String message;

	public Integer getCode() {
		return code;
	}
	
	public AuthenticationErrorBean(AuthenticationError error) {
		this.code = error.getErrorCode().getErrorCode();
		this.message = error.getMessage();
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
