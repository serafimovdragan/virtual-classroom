package virtual.classroom.util.errorhandling;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AuthenticationErrorHandler implements ExceptionMapper<AuthenticationError> {

	public Response toResponse(AuthenticationError exception) {
		AuthenticationErrorBean bean = new AuthenticationErrorBean(exception);
		return Response.serverError().entity(bean).build();
	}

}
