/**
 * 
 */
package virtual.classroom.util.errorhandling;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Dragan
 *
 */
public class RestClientErrorBean {
	
	@JsonProperty("code")
	private Integer code;
	
	@JsonProperty("msg")
	private String message;

	/**
	 * 
	 */
	public RestClientErrorBean(RestClientError error) {
		this.message = error.getMessage();
		this.code = error.getErrorCode().getErrorCode();
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
