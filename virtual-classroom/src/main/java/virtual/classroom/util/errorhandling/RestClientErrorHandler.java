/**
 * 
 */
package virtual.classroom.util.errorhandling;

import javax.ws.rs.ext.Provider;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * @author Dragan
 *
 */
@Provider
public class RestClientErrorHandler implements ExceptionMapper<RestClientError> {

	public Response toResponse(RestClientError exception) {
		RestClientErrorBean bean = new RestClientErrorBean(exception);
		return Response.serverError().entity(bean).build();
	}

}
