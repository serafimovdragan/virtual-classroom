/**
 * 
 */
package virtual.classroom.util.errorhandling;

/**
 * @author Dragan
 *
 */
public enum ErrorCode {

	// Rest client error
	PropertyDoesNotExist(0, "Property with key %1$s does not exist"),
	PasswordsDontMatch(1, "Passwords don't match"),
	UsernameOrPasswordNotInserted(2, "Username or password not inserted"),
	ProfessorTypeNotSpecified(3, "Professor type not specified"),
	CanNotHaveMoreThanOneAnswer(4, "This question '%1$s', can not have more than one answer"),
	NoCorrectAnswer(5, "This question '%1$s' must have a correct answer"),
	MustHaveOneCorrectAnswer(6, "This question '%1$s' must have only one correct question"),
	ValueCanNotBeNull(7, "%1$s can not be null"),
	NotEnoughCredits(8, "Not enough credits to enroll in course"),
	InstanceExists(9, "Examination instance with id '%1$s' and name '%2$s' already exists!"),
	QuestionInstanceExists(10, "Question instance with id '%1$s' and instance '%2$s' already exists!"),
	ExaminationInstanceDoesNotExist(11, "Examination instance '%1$s' does not exist!"),
	AnswerAlreadySubmitted(12, "Answer already submitted!"),
	CouldNotUploadPicture(13, "Could not upload picture."),
	
	// Authentication error
	NotFound(100, "%1$s Not Found"),
	PasswordIncorrect(102, "Password incorrect"),
	UserExists(103, "Professor with username %1$s already exists");
	
	private Integer errorCode;
	private String message;

	private ErrorCode(String message) {
		this.message = message;
		this.errorCode = this.ordinal();
	}

	private ErrorCode(int errorCode, String message) {
		this.errorCode = errorCode;
		this.message = message;

	}

	/**
	 * @return the errorCode
	 */
	public Integer getErrorCode() {

		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
