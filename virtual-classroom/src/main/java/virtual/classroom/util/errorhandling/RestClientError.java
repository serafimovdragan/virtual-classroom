/**
 * 
 */
package virtual.classroom.util.errorhandling;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dragan
 *
 */
public class RestClientError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ErrorCode errorCode;
	
	private List<Object> parameters;
	
	private String message;

	public RestClientError(ErrorCode errorCode, Object... parameters) {
		super(errorCode.getMessage());
		
		this.errorCode = errorCode;
		
		this.parameters = new ArrayList<Object>();
		for (Object object : parameters) {
			this.parameters.add(object);
		}
		
		this.message = getMessage();
	}
	
	public RestClientError(Integer statusCode, String message) {
		super(message);
	}

	public List<Object> getParameters() {
		return parameters;
	}

	public void setParameters(List<Object> parameters) {
		this.parameters = parameters;
	}

	public String getMessage() {
		this.message = this.errorCode.getMessage();
		for(int i = 0; i < this.parameters.size(); i ++) {
			message = message.replaceAll("%" + (i + 1) + "\\$s", parameters.get(i) + "");
		}
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}
}
