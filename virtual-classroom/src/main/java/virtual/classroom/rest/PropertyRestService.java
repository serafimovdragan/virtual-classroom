/**
 * 
 */
package virtual.classroom.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.ejb3.annotation.SecurityDomain;

import virtual.classroom.bean.ApplicationPropertyBean;
import virtual.classroom.constant.Constants;
import virtual.classroom.service.PropertyService;
import virtual.classroom.util.errorhandling.RestClientError;

/**
 * @author Dragan
 *
 */
@Path("/property")
@SecurityDomain("virtual-classroom")
public class PropertyRestService {

	@EJB
	private PropertyService propertyService;
	
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({Constants.ROLE_ADMIN, Constants.ROLE_PROFESSOR})
	public List<ApplicationPropertyBean> getProperty(@QueryParam("key") String key) throws RestClientError {
		if(key != null && !key.equals("")) {
			return new ArrayList<ApplicationPropertyBean>(Arrays.asList(propertyService.getProperty(key)));
		}
		return propertyService.getProperties();
	}
	
	@POST
	@Path("/put")
	public Response putProperty(@QueryParam("key")String key, @QueryParam("value")String value) {
		propertyService.putProperty(key, value);
		
		return Response.ok().build();
	}
}
