/**
 * 
 */
package virtual.classroom.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Dragan
 *
 */
@ApplicationPath("api")
public class MyApplication extends Application{

}
