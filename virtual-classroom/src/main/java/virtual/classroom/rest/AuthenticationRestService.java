/**
 * 
 */
package virtual.classroom.rest;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.ejb3.annotation.SecurityDomain;

import virtual.classroom.bean.AdminBean;
import virtual.classroom.bean.AdminSessionBean;
import virtual.classroom.bean.ProfessorBean;
import virtual.classroom.bean.ProfessorSessionBean;
import virtual.classroom.bean.StudentBean;
import virtual.classroom.bean.StudentSessionBean;
import virtual.classroom.service.AuthenticationService;
import virtual.classroom.util.errorhandling.AuthenticationError;
import virtual.classroom.util.errorhandling.ErrorCode;
import virtual.classroom.util.errorhandling.RestClientError;

/**
 * @author Dragan
 *
 */
@Path("/auth")
@SecurityDomain("virtual-classroom")
public class AuthenticationRestService {

	private final static String AUTHORIZATION_HEADER = "Authorization";
	
	@EJB
	private AuthenticationService authenticationService;
	
	@Context
	private HttpServletRequest httpRequest;
	
	@POST
	@Path("/admin/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public AdminSessionBean adminLogin(AdminBean adminBean) throws RestClientError, NoSuchAlgorithmException, InvalidKeySpecException, AuthenticationError {
		if(adminBean.getUsername() == null || adminBean.getPassword() == null)
			throw new RestClientError(ErrorCode.UsernameOrPasswordNotInserted);
		
		return authenticationService.adminLogin(adminBean);
	}
	
	@POST
	@Path("/admin/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response adminLogout() throws AuthenticationError {
		String session = httpRequest.getHeader(AUTHORIZATION_HEADER);
		
		authenticationService.adminLogout(session);
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/professor/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProfessorSessionBean professorLogin(ProfessorBean professorBean) throws RestClientError, AuthenticationError, NoSuchAlgorithmException, InvalidKeySpecException, ParseException {
		if(professorBean.getUsername() == null || professorBean.getPassword() == null)
			throw new RestClientError(ErrorCode.UsernameOrPasswordNotInserted);
		
		if(professorBean.getType() == null)
			throw new RestClientError(ErrorCode.ProfessorTypeNotSpecified);
		
		return authenticationService.professorLogin(professorBean);
	}
	
	@POST
	@Path("/professor/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response professorLogout() throws AuthenticationError {
		String session = httpRequest.getHeader(AUTHORIZATION_HEADER);
		
		authenticationService.professorLogout(session);
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/student/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public StudentSessionBean studentLogin(StudentBean studentBean) throws RestClientError, NoSuchAlgorithmException, InvalidKeySpecException, AuthenticationError {
		if(studentBean.getUsername() == null || studentBean.getPassword() == null) 
			throw new RestClientError(ErrorCode.UsernameOrPasswordNotInserted); 
		
		return authenticationService.studentLogin(studentBean);
	}
	
	@POST
	@Path("/student/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response studentLogout() throws AuthenticationError {
		String session = httpRequest.getHeader(AUTHORIZATION_HEADER);
		
		authenticationService.studentLogout(session);
		
		return Response.ok().build();
	}
}
