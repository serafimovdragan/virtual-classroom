/**
 * 
 */
package virtual.classroom.rest;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.ejb3.annotation.SecurityDomain;

import virtual.classroom.bean.CourseBean;
import virtual.classroom.bean.ExaminationBean;
import virtual.classroom.constant.Constants;
import virtual.classroom.service.ProfessorService;

/**
 * @author Dragan
 *
 */
@Path("/professor")
@SecurityDomain("virtual-classroom")
public class ProfessorRestService {

	@EJB
	private ProfessorService professorService;
	
	@GET
	@Path("/courses")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public List<CourseBean> getCourses(@QueryParam("professorId") Integer professorId) {
		if(professorId == null) {
			return null;
		}
		
		return professorService.getCourses(professorId);
	}
	
	@GET
	@Path("courses/examinations")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public List<ExaminationBean> getExaminations(@QueryParam("courseId") Integer courseId) {
		if(courseId == null) {
			return null;
		}
		
		return professorService.getExaminations(courseId);
	}
}
