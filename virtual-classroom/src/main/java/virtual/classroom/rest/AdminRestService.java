/**
 * 
 */
package virtual.classroom.rest;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.ejb3.annotation.SecurityDomain;

import virtual.classroom.bean.AdminBean;
import virtual.classroom.bean.CourseBean;
import virtual.classroom.bean.ProfessorBean;
import virtual.classroom.bean.StudentBean;
import virtual.classroom.constant.Constants;
import virtual.classroom.service.AdminService;
import virtual.classroom.util.errorhandling.AuthenticationError;
import virtual.classroom.util.errorhandling.ErrorCode;
import virtual.classroom.util.errorhandling.RestClientError;

/**
 * @author Dragan
 *
 */
@Path("/admin")
@Stateless
@SecurityDomain("virtual-classroom")
public class AdminRestService {

	@EJB
	private AdminService adminService;
	
	@POST
	@Path("/admin/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response insertAdmin(@Valid AdminBean adminBean) throws RestClientError, NoSuchAlgorithmException, InvalidKeySpecException {
		if(!adminBean.getPassword().equals(adminBean.getRepeatPassword())) {
			throw new RestClientError(ErrorCode.PasswordsDontMatch);
		}
		
		adminService.insertAdmin(adminBean);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/admin/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response editAdmin(@Valid AdminBean adminBean) throws AuthenticationError, RestClientError{
		if(adminBean.getId() == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		
		adminService.editAdmin(adminBean);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/admin/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response deleteAdmin(@QueryParam("id") Integer adminId) throws RestClientError {
		if(adminId == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		
		adminService.deleteAdmin(adminId);
		return Response.ok().build();
	}
	
	@GET
	@Path("/admin/get")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AdminBean> getAdmin(@QueryParam("id") Integer adminId) {
		return adminService.getAdmin(adminId);
	}
	
	@POST
	@Path("/professor/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response insertProfessor(@Valid ProfessorBean professorBean) throws RestClientError, NoSuchAlgorithmException, InvalidKeySpecException, AuthenticationError {
		if(!professorBean.getPassword().equals(professorBean.getRepeatPassword())) {
			throw new RestClientError(ErrorCode.PasswordsDontMatch);
		}
		
		adminService.insertProfessor(professorBean);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/professor/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response editProfessor(@Valid ProfessorBean professorBean) throws AuthenticationError, RestClientError {
		if(professorBean.getId() == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		
		adminService.editProfessor(professorBean);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/professor/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response deleteProfessor(@QueryParam("id") Integer professorId) throws RestClientError {
		if(professorId == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		
		adminService.deleteProfessor(professorId);
		return Response.ok().build();
	}
	
	@GET
	@Path("/professor/get")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProfessorBean> getProfessor(@QueryParam("id") Integer professorId) {
		return adminService.getprofessor(professorId);
	}
	
	@POST
	@Path("/student/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response insertStudent(@Valid StudentBean studentBean) throws NoSuchAlgorithmException, InvalidKeySpecException {
		adminService.insertStudent(studentBean);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/student/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response editStudent(@Valid StudentBean studentBean) throws AuthenticationError, RestClientError {
		if(studentBean.getId() == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		
		adminService.editStudent(studentBean);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/student/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response deleteStudent(@QueryParam("id") Integer studentId) throws RestClientError {
		if(studentId == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		
		adminService.deleteStudent(studentId);
		return Response.ok().build();
	}
	
	@GET
	@Path("/student/get")
	@Produces(MediaType.APPLICATION_JSON)
	public List<StudentBean> getStudent(@QueryParam("id") Integer studentId) {
		return adminService.getStudent(studentId);
	}
	
	@POST
	@Path("/course/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response insertCourse(@Valid CourseBean courseBean) {
		adminService.insertCourse(courseBean);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/course/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response editCourse(@Valid CourseBean courseBean) throws RestClientError, AuthenticationError {
		if(courseBean.getId() == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		adminService.editCourse(courseBean);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/course/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public Response deleteCourse(@QueryParam("id") Integer courseId) throws RestClientError {
		if(courseId == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "id");
		}
		
		adminService.deleteCourse(courseId);
		return Response.ok().build();
	}
	
	@GET
	@Path("/course/get")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_ADMIN)
	public List<CourseBean> getCourse(@QueryParam("id") Integer courseId) {
		return adminService.getCourse(courseId);
	}
}
