/**
 * 
 */
package virtual.classroom.rest;

import java.io.File;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.resteasy.plugins.providers.FileProvider;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import virtual.classroom.bean.ExaminationBean;
import virtual.classroom.bean.ExaminationInstanceBean;
import virtual.classroom.bean.QuestionBean;
import virtual.classroom.bean.QuestionInstanceBean;
import virtual.classroom.constant.Constants;
import virtual.classroom.service.ExaminationService;
import virtual.classroom.util.errorhandling.ErrorCode;
import virtual.classroom.util.errorhandling.RestClientError;

@Path("/examination/")
@SecurityDomain("virtual-classroom")
public class ExaminationRestService {

	@EJB
	private ExaminationService examinationService;
	
	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public Response insertExamination(@Valid ExaminationBean examinationBean) throws RestClientError {
		examinationService.insertExamination(examinationBean);
		
		return Response.ok().build();
	}

	@GET
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public Response deleteExamination(@QueryParam("id") Integer id) throws RestClientError {
		examinationService.deleteExamination(id);
		return Response.ok().build();
	}
	
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public ExaminationBean getExamination(@QueryParam("id") Integer examinationId) {
		return examinationService.getExamination(examinationId);
	}
	
	@GET
	@Path("instance/get")
	@Produces(MediaType.APPLICATION_JSON)
	public ExaminationInstanceBean getExaminationInstance(@QueryParam("id") Integer examinationId, @QueryParam("instance") String instance) {
		return examinationService.getExaminationInstance(examinationId, instance);
	}
	
	@GET
	@Path("instances/get")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public List<ExaminationInstanceBean> getExaminationInstances(@QueryParam("examinationId") Integer examinationId, @QueryParam("professorId") Integer professorId) {
		return examinationService.getExaminationInstances(examinationId, professorId);
	}
	
	@GET
	@Path("/activate")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public Response activateExamination(@QueryParam("professorId") Integer professorId, @QueryParam("id") Integer examinationId, @QueryParam("instance") String instance) throws RestClientError {
		if(instance == null || instance.equals("")) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "instance");
		}
		
		examinationService.activateExamination(professorId, examinationId, instance);
		return Response.ok().build();
	}
	
	@GET
	@Path("question/activate")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public Response activateQuestion(@QueryParam("examinationId") Integer examinationId, @QueryParam("instance") String instance, @QueryParam("questionId") Integer questionId, @QueryParam("duration") Integer duration, @QueryParam("seeResults") Boolean seeResults) throws RestClientError {
		if(examinationId == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "examination id");
		}
		if(instance == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "instance");
		}
		if(questionId == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "question id");
		}
		if(duration == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "duration");
		}
		if(seeResults == null) {
			seeResults = false;
		}
		
		examinationService.activateQuestion(examinationId, instance, questionId, duration, seeResults);
		return Response.ok().build();
	}
	
	@GET
	@Path("question/results")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public QuestionBean getResults(@QueryParam("instance") String instance, @QueryParam("id") Integer questionId) throws RestClientError {
		if(instance == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "instance");
		}
		if(questionId == null) {
			throw new RestClientError(ErrorCode.ValueCanNotBeNull, "question id");
		}
		return examinationService.getResults(instance, questionId);
	}
	
	@POST
	@Path("/picture/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public String uploadPicture(MultipartFormDataInput picture) throws RestClientError {
		return examinationService.uploadPicture(picture);
	}

	@GET
	@Path("/picture")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getPicture(@QueryParam("pictureURL") String pictureURL) {
		File file = new File(pictureURL);
		return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
				.header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
				.build();
	}

	
	@GET
	@Path("/question/publish")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_PROFESSOR)
	public Response publishQuestion(@QueryParam("instance") String instance, @QueryParam("questionId") Integer questionId) {
		examinationService.publishQuestion(instance, questionId);
		return Response.ok().build();
	}
	
	@GET
	@Path("/question/isPublic")
	@Produces(MediaType.APPLICATION_JSON)
	public QuestionInstanceBean isQuestionPublic(@QueryParam("instance") String instance, @QueryParam("questionId") Integer questionId) {
		return examinationService.isQuestionPublic(instance, questionId);
	}
}
