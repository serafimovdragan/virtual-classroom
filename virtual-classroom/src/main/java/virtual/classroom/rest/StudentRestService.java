/**
 * 
 */
package virtual.classroom.rest;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.ejb3.annotation.SecurityDomain;

import virtual.classroom.bean.ExaminationBean;
import virtual.classroom.bean.SubmittedAnswerBean;
import virtual.classroom.constant.Constants;
import virtual.classroom.service.StudentService;
import virtual.classroom.util.errorhandling.RestClientError;

/**
 * @author Dragan
 *
 */
@Path("/student")
@SecurityDomain("virtual-classroom")
public class StudentRestService {

	@EJB
	private StudentService studentService;
	
	@POST
	@Path("/enroll")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_STUDENT)
	public Response enroll(@QueryParam("studentId") Integer studentId, @QueryParam("courseId") Integer courseId) throws RestClientError {
		studentService.enroll(studentId, courseId);
		return Response.ok().build();
	}
	
	@GET
	@Path("/examinations")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_STUDENT)
	public List<ExaminationBean> getActiveExaminations(@QueryParam("studentId") Integer studentId) {
		return studentService.getActiveExaminations(studentId);
	}
	
	@GET
	@Path("/examination")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(Constants.ROLE_STUDENT)
	public ExaminationBean getExamination(@QueryParam("name") String name) throws RestClientError {
		return studentService.getExamination(name);
	}
	
	@POST
	@Path("/examination/start")
	@Produces(MediaType.APPLICATION_JSON)
	public Response startExamination(@QueryParam("examinationId") Integer examinationId, @QueryParam("studentId") Integer studentId) {
		studentService.startExamination(examinationId, studentId);
		return Response.ok().build();
	}
	
	@POST
	@Path("/examination/submit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Integer submitExamination(@Context HttpServletRequest requestContext, List<SubmittedAnswerBean> submittedAnswerBeans) throws RestClientError {
		return studentService.submitExamination(submittedAnswerBeans, requestContext.getRemoteAddr());
	}
}
