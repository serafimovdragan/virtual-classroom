/**
 * 
 */
package virtual.classroom.bean;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import virtual.classroom.entity.Question.QuestionType;

/**
 * @author Dragan
 *
 */
public class QuestionBean {

	private Integer id;
	
	private Integer examinationId;
	
	@NotNull
	@NotBlank
	private String question;

	private String pictureURL;
	
	private Integer duration;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd HH:mm:ss", timezone = "CET")
	private Date expirationDate;
	
	private boolean active;
	
	@NotNull
	private QuestionType type;
	
	private Boolean seeResults = false;
	
	private Set<AnswerBean> answers = new HashSet<AnswerBean>();
	
	private Set<SubmittedAnswerBean> submittedAnswers = new HashSet<SubmittedAnswerBean>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getExaminationId() {
		return examinationId;
	}

	public void setExaminationId(Integer examinationId) {
		this.examinationId = examinationId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Set<AnswerBean> getAnswers() {
		return answers;
	}

	public void setAnswers(Set<AnswerBean> answers) {
		this.answers = answers;
	}

	public QuestionType getType() {
		return type;
	}

	public void setType(QuestionType type) {
		this.type = type;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Set<SubmittedAnswerBean> getSubmittedAnswers() {
		return submittedAnswers;
	}

	public void setSubmittedAnswers(Set<SubmittedAnswerBean> submittedAnswers) {
		this.submittedAnswers = submittedAnswers;
	}

	public Boolean getSeeResults() {
		return seeResults;
	}

	public void setSeeResults(Boolean seeResults) {
		this.seeResults = seeResults;
	}

	public String getPictureURL() {
		return pictureURL;
	}

	public void setPictureURL(String pictureURL) {
		this.pictureURL = pictureURL;
	}
}
