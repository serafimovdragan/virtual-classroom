/**
 * 
 */
package virtual.classroom.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Dragan
 *
 */
public class QuestionInstanceBean {

	private Integer id;
	
	private Integer examinationInstanceId;
	
	private Integer questionId;
	
	private Integer duration;
	
	private Boolean seeResults;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "CET")
	private Date activationDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "CET")
	private Date expirationDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getExaminationInstanceId() {
		return examinationInstanceId;
	}

	public void setExaminationInstanceId(Integer examinationInstanceId) {
		this.examinationInstanceId = examinationInstanceId;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean getSeeResults() {
		return seeResults;
	}

	public void setSeeResults(Boolean seeResults) {
		this.seeResults = seeResults;
	}
	
}
