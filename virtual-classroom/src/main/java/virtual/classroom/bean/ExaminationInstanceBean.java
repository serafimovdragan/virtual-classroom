/**
 * 
 */
package virtual.classroom.bean;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Dragan
 *
 */
public class ExaminationInstanceBean {

	private Integer id;
	
	private String name;
	
	private Integer examinationId;
	
	private ExaminationBean examination;
	
	private Integer profesorId;
	
	private Set<QuestionInstanceBean> questions = new HashSet<QuestionInstanceBean>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExaminationId() {
		return examinationId;
	}

	public void setExaminationId(Integer examinationId) {
		this.examinationId = examinationId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ExaminationBean getExamination() {
		return examination;
	}

	public void setExamination(ExaminationBean examination) {
		this.examination = examination;
	}

	public Integer getProfesorId() {
		return profesorId;
	}

	public void setProfesorId(Integer profesorId) {
		this.profesorId = profesorId;
	}

	public Set<QuestionInstanceBean> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<QuestionInstanceBean> questions) {
		this.questions = questions;
	}
	
}
