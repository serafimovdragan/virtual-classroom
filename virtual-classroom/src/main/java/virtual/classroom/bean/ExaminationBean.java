/**
 * 
 */
package virtual.classroom.bean;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import virtual.classroom.entity.Examination.ExaminationType;

/**
 * @author Dragan
 *
 */
public class ExaminationBean {

	private Integer id;
	
	@NotNull
	private ExaminationType type;
	
	private Integer professorId;
	
	private ProfessorBean professor;
	
	@NotNull
	@NotBlank
	private String name;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "CET")
	private Date creationDate;

	private Set<QuestionBean> questions = new HashSet<QuestionBean>();
	
	@NotNull
	private Integer courseId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ExaminationType getType() {
		return type;
	}

	public void setType(ExaminationType type) {
		this.type = type;
	}

	public ProfessorBean getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorBean professor) {
		this.professor = professor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getProfessorId() {
		return professorId;
	}

	public void setProfessorId(Integer professorId) {
		this.professorId = professorId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Set<QuestionBean> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<QuestionBean> questions) {
		this.questions = questions;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
}
