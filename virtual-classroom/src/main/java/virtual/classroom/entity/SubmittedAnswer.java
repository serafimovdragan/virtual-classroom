/**
 * 
 */
package virtual.classroom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.ws.rs.core.Context;

import virtual.classroom.entity.Question.QuestionType;

/**
 * @author Dragan
 *
 */
@Entity
@Table(name = "answers_submitted")
@NamedQueries({
	@NamedQuery(name = "SubmittedAnswer.answerExists", query = "SELECT sa FROM SubmittedAnswer sa WHERE sa.ipAdress = :ipAdress AND sa.instance = :instance AND sa.questionId = :questionId"),
	@NamedQuery(name = "SubmittedAnswer.getAnswers", query = "SELECT sa FROM SubmittedAnswer sa WHERE sa.instance = :instance AND sa.questionId = :questionId")
})
public class SubmittedAnswer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "student_id")
	private Integer studentId;
	
	@Column(name = "examination_id")
	private Integer examinationId;
	
	@Column(name = "instance")
	private String instance;
	
	@Column(name = "question_id")
	private Integer questionId;
	
	@Column(name = "question_type")
	private QuestionType questionType;
	
	@Column(name = "answer_id")
	private Integer answerId;
	
	@Column(name = "answer")
	private String answer;
	
	@Column(name = "points")
	private Integer points;

	@Column(name = "ip_adress")
	private String ipAdress;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public Integer getExaminationId() {
		return examinationId;
	}

	public void setExaminationId(Integer examinationId) {
		this.examinationId = examinationId;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Integer getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}
}
