/**
 * 
 */
package virtual.classroom.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Dragan
 *
 */
@Entity
@Table(name = "examinations")
@NamedQueries({
	@NamedQuery(name = "Examination.getByCourses", query = "SELECT e FROM Examination e WHERE e.courseId in :courseIds and e.deleted = false"),
	@NamedQuery(name = "Examination.getByName", query = "SELECT e FROM Examination e WHERE e.name = :name")
})
public class Examination {

	public enum ExaminationType {
		Survey, Test
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "type")
	private ExaminationType type;

	@Column(name = "professor_id")
	private Integer professorId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "professor_id", insertable = false, updatable = false)
	private Professor professor;

	@Column(name = "name")
	private String name;

	@Column(name = "creation_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "examination")
	private Set<Question> questions = new HashSet<Question>();
	
	@Column(name = "course_id")
	private Integer courseId;

	@Column(name = "deleted")
	private Boolean deleted = false;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ExaminationType getType() {
		return type;
	}

	public void setType(ExaminationType type) {
		this.type = type;
	}

	public Integer getProfessorId() {
		return professorId;
	}

	public void setProfessorId(Integer professorId) {
		this.professorId = professorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Set<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
}
