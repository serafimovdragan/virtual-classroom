/**
 * 
 */
package virtual.classroom.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Dragan
 *
 */
@Entity
@Table(name = "questions_instances")
public class QuestionInstance {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "examination_instance_id")
	private Integer examinationInstanceId;
	
	@Column(name = "question_id")
	private Integer questionId;
	
	@Column(name = "duration")
	private Integer duration;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "examination_instance_id", insertable = false, updatable = false)
	private ExaminationInstance examinationInstance;
	
	@Column(name = "activation_date")
	private Date activationDate;
	
	@Column(name = "expiration_date")
	private Date expirationDate;
	
	@Column(name = "see_results")
	private Boolean seeResults;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getExaminationInstanceId() {
		return examinationInstanceId;
	}

	public void setExaminationInstanceId(Integer examinationInstanceId) {
		this.examinationInstanceId = examinationInstanceId;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public ExaminationInstance getExaminationInstance() {
		return examinationInstance;
	}

	public void setExaminationInstance(ExaminationInstance examinationInstance) {
		this.examinationInstance = examinationInstance;
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean getSeeResults() {
		return seeResults;
	}

	public void setSeeResults(Boolean seeResults) {
		this.seeResults = seeResults;
	}
	
}
