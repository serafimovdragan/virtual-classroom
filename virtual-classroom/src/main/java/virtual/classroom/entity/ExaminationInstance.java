/**
 * 
 */
package virtual.classroom.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Dragan
 *
 */
@Entity
@Table(name = "examinations_instances")
@NamedQueries({
	@NamedQuery(name = "ExaminationInstance.findByInstance", query = "SELECT ei FROM ExaminationInstance ei WHERE ei.examinationId = :examinationId and ei.name = :instance"),
	@NamedQuery(name = "ExaminationInstance.findOnlyByInstance", query = "SELECT ei FROM ExaminationInstance ei WHERE ei.name = :instance"),
	@NamedQuery(name = "ExaminationInstance.findByProfessor", query = "SELECT ei FROM ExaminationInstance ei WHERE ei.examinationId = :examinationId and ei.professorId = :professorId")
})
public class ExaminationInstance {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "examination_id")
	private Integer examinationId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "examination_id", insertable = false, updatable = false)
	private Examination examination;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "examinationInstance")
	private Set<QuestionInstance> questionInstances = new HashSet<QuestionInstance>();
	
	@Column(name = "professor_id")
	private Integer professorId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "professor_id", insertable = false, updatable = false)
	private Professor professor;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExaminationId() {
		return examinationId;
	}

	public void setExaminationId(Integer examinationId) {
		this.examinationId = examinationId;
	}

	public Examination getExamination() {
		return examination;
	}

	public void setExamination(Examination examination) {
		this.examination = examination;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<QuestionInstance> getQuestionInstances() {
		return questionInstances;
	}

	public void setQuestionInstances(Set<QuestionInstance> questionInstances) {
		this.questionInstances = questionInstances;
	}

	public Integer getProfessorId() {
		return professorId;
	}

	public void setProfessorId(Integer professorId) {
		this.professorId = professorId;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
}
