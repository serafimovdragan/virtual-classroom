/**
 * 
 */
package virtual.classroom.entity;

import java.io.Serializable;

/**
 * @author Dragan
 *
 */
public class StudentEnrollmentID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7908846834977499539L;

	private Integer studentId;
	
	private Integer courseId;

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
}
