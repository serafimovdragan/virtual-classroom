/**
 * 
 */
package virtual.classroom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "students_courses")
@IdClass(StudentEnrollmentID.class)
@NamedQueries({
	@NamedQuery(name = "StudentEnrollment.getByStudent", query = "SELECT se FROM StudentEnrollment se where se.studentId = :studentId"),
	@NamedQuery(name = "StudentEnrollment.getCourseIdsByStudent", query = "SELECT se.courseId FROM StudentEnrollment se where se.studentId = :studentId")
})
public class StudentEnrollment {

	@Id
	@Column(name = "student_id")
	private Integer studentId;
	
	@Id
	@Column(name = "course_id")
	private Integer courseId;

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
}
