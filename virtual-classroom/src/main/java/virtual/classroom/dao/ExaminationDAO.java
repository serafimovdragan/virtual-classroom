package virtual.classroom.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.Examination;

@Stateless
public class ExaminationDAO extends GenericDAO<Examination, Integer> {

	protected ExaminationDAO() {
		super(Examination.class);
	}

	@SuppressWarnings("unchecked")
	public List<Examination> getByCourses(List<Integer> courseIds) {
		Query query = getEntityManager().createNamedQuery("Examination.getByCourses");
		query.setParameter("courseIds", courseIds);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public Examination find(String name) {
		Query query = getEntityManager().createNamedQuery("Examination.getByName");
		query.setParameter("name", name);
		List<Examination> result = query.getResultList();
		if(result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	public void deleteExamination(Integer id) {
		Query query = getEntityManager().createNativeQuery("UPDATE examinations SET deleted = true WHERE id = " + id);
		query.executeUpdate();
	}

    public void deleteExaminationByName(String name) {
        Query query = getEntityManager().createNativeQuery("UPDATE examinations SET deleted = true WHERE name = \'" + name + "\'");
        query.executeUpdate();
    }
}
