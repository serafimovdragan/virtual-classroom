/**
 * 
 */
package virtual.classroom.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.SubmittedAnswer;

/**
 * @author Dragan
 *
 */
@Stateless
public class SubmittedAnswerDAO extends GenericDAO<SubmittedAnswer, Integer> {

	protected SubmittedAnswerDAO() {
		super(SubmittedAnswer.class);
	}

	@SuppressWarnings("unchecked")
	public boolean alreadyAnswered(SubmittedAnswer submittedAnswer) {
		Query query = getEntityManager().createNamedQuery("SubmittedAnswer.answerExists");
		
		query.setParameter("ipAdress", submittedAnswer.getIpAdress());
		query.setParameter("instance", submittedAnswer.getInstance());
		query.setParameter("questionId", submittedAnswer.getQuestionId());
		
		List<SubmittedAnswer> result = query.getResultList();
		
		if(result == null || result.isEmpty()) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<SubmittedAnswer> getAnswers(String instance, Integer questionId) {
		Query query = getEntityManager().createNamedQuery("SubmittedAnswer.getAnswers");
		
		query.setParameter("instance", instance);
		query.setParameter("questionId", questionId);
		
		return query.getResultList();
	}

}
