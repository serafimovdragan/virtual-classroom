/**
 * 
 */
package virtual.classroom.dao;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.Professor;

/**
 * @author Dragan
 *
 */
@Stateless
public class ProfessorDAO extends GenericDAO<Professor, Integer> {

	protected ProfessorDAO() {
		super(Professor.class);
	}

	public Professor find(String username) {
		Query query = getEntityManager().createNamedQuery("Professor.findByUsername");
		query.setParameter("username", username);
		
		try {
			return (Professor) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
