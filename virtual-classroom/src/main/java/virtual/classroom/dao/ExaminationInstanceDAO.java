/**
 * 
 */
package virtual.classroom.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.ExaminationInstance;

/**
 * @author Dragan
 *
 */
@Stateless
public class ExaminationInstanceDAO extends GenericDAO<ExaminationInstance, Integer> {

	protected ExaminationInstanceDAO() {
		super(ExaminationInstance.class);
	}
	
	public ExaminationInstance findByInstance(Integer examinationId, String instance) {
		Query query = getEntityManager().createNamedQuery("ExaminationInstance.findByInstance");
		
		query.setParameter("examinationId", examinationId);
		query.setParameter("instance", instance);
		
		try {
			return (ExaminationInstance) query.getResultList().iterator().next();
		} catch (Exception e) {
			return null;
		}
	}
	
	public ExaminationInstance findByInstance(String instance) {
		Query query = getEntityManager().createNamedQuery("ExaminationInstance.findOnlyByInstance");
		
		query.setParameter("instance", instance);
		
		try {
			return (ExaminationInstance) query.getResultList().iterator().next();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExaminationInstance> findByProfessor(Integer examinationId, Integer professorId) {
		Query query = getEntityManager().createNamedQuery("ExaminationInstance.findByProfessor");
		
		query.setParameter("examinationId", examinationId);
		query.setParameter("professorId", professorId);
		
		return query.getResultList();
	}

}
