/**
 * 
 */
package virtual.classroom.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.AdminSession;

/**
 * @author Dragan
 *
 */
@Stateless
public class AdminSessionDAO extends GenericDAO<AdminSession, Integer>{

	protected AdminSessionDAO() {
		super(AdminSession.class);
	}

	@SuppressWarnings("unchecked")
	public AdminSession findActive(Integer adminId) {
		Query query = getEntityManager().createNamedQuery("AdminSession.findActive");
		query.setParameter("adminId", adminId);

		// should be only one
		List<AdminSession> activeSessions = (List<AdminSession>) query.getResultList();

		if (activeSessions == null || activeSessions.isEmpty()) {
			return null;
		}
		return activeSessions.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public AdminSession validate(String session) {
		Query query = getEntityManager().createNamedQuery("AdminSession.validate");
		query.setParameter("session", session);
		query.setParameter("now", new Date());
		
		List<AdminSession> activeSessions = query.getResultList();
		
		if(activeSessions == null || activeSessions.isEmpty())
			return null;
		
		return activeSessions.get(0);
	}

}
