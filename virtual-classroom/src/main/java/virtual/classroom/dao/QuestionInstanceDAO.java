/**
 * 
 */
package virtual.classroom.dao;

import javax.ejb.Stateless;

import virtual.classroom.entity.QuestionInstance;

/**
 * @author Dragan
 *
 */
@Stateless
public class QuestionInstanceDAO extends GenericDAO<QuestionInstance, Integer> {

	protected QuestionInstanceDAO() {
		super(QuestionInstance.class);
	}
	
}
