package virtual.classroom.dao;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.Admin;

@Stateless
public class AdminDAO extends GenericDAO<Admin, Integer>{

	protected AdminDAO() {
		super(Admin.class);
	}

	public Admin find(String username) {
		Query query = getEntityManager().createNamedQuery("Admin.findByUsername");
		query.setParameter("username", username);
		
		try {
			return (Admin) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
