/**
 * 
 */
package virtual.classroom.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.ProfessorSession;

/**
 * @author Dragan
 *
 */
@Stateless
public class ProfessorSessionDAO extends GenericDAO<ProfessorSession, Integer> {

	protected ProfessorSessionDAO() {
		super(ProfessorSession.class);
	}

	@SuppressWarnings("unchecked")
	public ProfessorSession findActive(Integer professorId) {
		Query query = getEntityManager().createNamedQuery("ProfessorSession.findActive");
		query.setParameter("professorId", professorId);

		// should be only one
		List<ProfessorSession> activeSessions = (List<ProfessorSession>) query.getResultList();

		if (activeSessions == null || activeSessions.isEmpty()) {
			return null;
		}
		return activeSessions.get(0);
	}

	@SuppressWarnings("unchecked")
	public ProfessorSession validate(String session) {
		Query query = getEntityManager().createNamedQuery("ProfessorSession.validate");
		query.setParameter("session", session);
		query.setParameter("now", new Date());
		// should be only one
		List<ProfessorSession> activeSessions = (List<ProfessorSession>) query.getResultList();

		if (activeSessions == null || activeSessions.isEmpty())
			return null;
			
		return activeSessions.get(0);
	}

}
