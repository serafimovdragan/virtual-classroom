/**
 * 
 */
package virtual.classroom.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.Answer;

/**
 * @author Dragan
 *
 */
@Stateless
public class AnswerDAO extends GenericDAO<Answer, Integer>{

	protected AnswerDAO() {
		super(Answer.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Answer> getAnswers(Integer questionId) {
		Query query = getEntityManager().createNamedQuery("Answer.getAnswers");
		
		query.setParameter("questionId", questionId);
		return query.getResultList();
	}

}
