/**
 * 
 */
package virtual.classroom.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.Course;

/**
 * @author Dragan
 *
 */
@Stateless
public class CourseDAO extends GenericDAO<Course, Integer> {

	protected CourseDAO() {
		super(Course.class);
	}

	@SuppressWarnings("unchecked")
	public List<Course> findByProfessor(Integer professorId) {
		Query query = getEntityManager().createNamedQuery("Course.findByProfessor");
		query.setParameter("professorId", professorId);
		return query.getResultList();
	}
}
