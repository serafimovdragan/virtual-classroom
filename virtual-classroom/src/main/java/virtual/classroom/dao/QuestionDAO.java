package virtual.classroom.dao;

import javax.ejb.Stateless;

import virtual.classroom.entity.Question;

@Stateless
public class QuestionDAO extends GenericDAO<Question, Integer> {

	protected QuestionDAO() {
		super(Question.class);
	}

}
