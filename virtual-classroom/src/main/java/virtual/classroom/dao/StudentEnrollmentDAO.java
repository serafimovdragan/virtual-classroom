/**
 * 
 */
package virtual.classroom.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.StudentEnrollment;
import virtual.classroom.entity.StudentEnrollmentID;

/**
 * @author Dragan
 *
 */
@Stateless
public class StudentEnrollmentDAO extends GenericDAO<StudentEnrollment, StudentEnrollmentID> {

	protected StudentEnrollmentDAO() {
		super(StudentEnrollment.class);
	}

	@SuppressWarnings("unchecked")
	public List<StudentEnrollment> getByStudent(Integer studentId) {
		Query query = getEntityManager().createNamedQuery("StudentEnrollment.getByStudent");
		query.setParameter("studentId", studentId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Integer> getCourseIdsByStudent(Integer studentId) {
		Query query = getEntityManager().createNamedQuery("StudentEnrollment.getCourseIdsByStudent");
		query.setParameter("studentId", studentId);
		return query.getResultList();
	}

}
