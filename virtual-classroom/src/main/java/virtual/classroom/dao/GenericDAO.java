/**
 * 
 */
package virtual.classroom.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * @author Dragan
 *
 */
public class GenericDAO<T, PK> {

	private Class<? extends T> entityClass;

	/**
	 * Entry point of the entity manager injected by the container (Application
	 * Server)
	 */
	@PersistenceContext(unitName = "virtual-classroom")
	protected EntityManager entityManager;

	public Integer getBatchSize() {
		return 50;
	}

	/**
	 * The main (and only) constructor.
	 * 
	 * @param entityClass
	 *            the type of entity represented by this DAO.
	 */
	protected GenericDAO(Class<? extends T> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * detach object from persistence context
	 * 
	 * @param t
	 * @return
	 */
	public T detach(final T t) {
		getEntityManager().detach(t);
		return t;
	}

	/**
	 * Puts a transient object into the persistent context.
	 * 
	 * @param t
	 *            the object entity to be associated with the persistent
	 *            context.
	 * @return the object entity after it has been associated with a persistent
	 *         context.
	 */
	public T create(final T t) {
		getEntityManager().persist(t);
		return t;
	}

	/**
	 * Delete an entity object from the database.
	 * 
	 * @param id
	 *            the primary key (identity) of the object.
	 */
	public void delete(final PK id) {
		getEntityManager().remove(getEntityManager().find(entityClass, id));
	}

	/**
	 * Load all entities from the database.
	 * 
	 * @return collection of loaded entities
	 */
	@SuppressWarnings("unchecked")
	public Collection<T> findAll() {
		return getEntityManager().createQuery(
				"FROM " + getEntityClass().getName()).getResultList();
	}

	@SuppressWarnings("unchecked")
	public Collection<T> findAll(int offset, int limit) {
		Query q = getEntityManager().createQuery(
				"FROM " + getEntityClass().getName());
		q.setFirstResult(offset);
		q.setMaxResults(limit);
		return q.getResultList();
	}

	/**
	 * Load an entity object from the database.
	 * 
	 * @param id
	 *            the primary key (identity) of the object.
	 * @return the loaded entity.
	 */
	public T find(final PK id) {
		return (T) getEntityManager().find(entityClass, id);
	}

	/**
	 * Select an entity for update
	 * 
	 * @param id
	 *            the id of the entity
	 * @return the entity if one was found
	 */
	public T findForUpdate(final PK id) {
		return (T) getEntityManager().find(entityClass, id,
				LockModeType.PESSIMISTIC_WRITE);
	}

	/**
	 * Gets the number of records in a table bound to a given entity
	 * 
	 * @return the number of records in the table for this entity
	 */
	public Integer count() {
		Query q = getEntityManager().createQuery(
				"SELECT  COUNT(s) as cn  FROM " + getEntityClass().getName()
						+ " s");
		Integer res = ((Number) q.getSingleResult()).intValue();
		return res;
	}

	/**
	 * Update/save an entity object in the database.
	 * 
	 * @param t
	 *            the entity object to be saved.
	 * @return the entity object after being saved.
	 */
	public T update(final T t) {
		return getEntityManager().merge(t);
	}

	/**
	 * Create or save entity
	 * 
	 * @param t
	 *            the entity to be saved
	 * @return the saved entity
	 */
	public T save(final T t) {
		if (!getEntityManager().contains(t)) {
			create(t);
		} else {
			return update(t);
		}
		return t;
	}

	/**
	 * Update the state of the object and discard all changes made to it.
	 * 
	 * @param t
	 *            the object to be reloaded.
	 */
	public void refresh(final T t) {
		getEntityManager().refresh(t);
	}

	/**
	 * @return the entity type of the object
	 */
	public Class<? extends T> getEntityClass() {
		return entityClass;
	}

	/**
	 * @return the persistence context of this DAO.
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager
	 *            the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public void insertAll(Collection<T> items) {
		// int batchSize=(Integer)
		// getEntityManager().getProperties().get("hibernate.jdbc.batch_size");
		int count = 0;
		for (T item : items) {
			getEntityManager().persist(item);
			if (++count % 50 == 0) { // 50, same as the JDBC batch size
				getEntityManager().flush();
				getEntityManager().clear();
			}
		}
		getEntityManager().flush();
	}
	
	public void flush() {
		getEntityManager().flush();
	}

	public void updateAll(Collection<T> items) {
		// int batchSize=(Integer)
		// getEntityManager().getProperties().get("hibernate.jdbc.batch_size");
		int count = 0;
		for (T item : items) {
			getEntityManager().merge(item);
			if (++count % 50 == 0) { // 50, same as the JDBC batch size
				getEntityManager().flush();
				getEntityManager().clear();
			}
		}
		getEntityManager().flush();
	}
	
	

}