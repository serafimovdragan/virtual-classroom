/**
 * 
 */
package virtual.classroom.dao;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.Student;

/**
 * @author Dragan
 *
 */
@Stateless
public class StudentDAO extends GenericDAO<Student, Integer> {

	protected StudentDAO() {
		super(Student.class);
	}

	public Student find(String username) {
		Query query = getEntityManager().createNamedQuery("Student.findByUsername");
		query.setParameter("username", username);
		
		try {
			return (Student) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
