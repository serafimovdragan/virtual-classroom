/**
 * 
 */
package virtual.classroom.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import virtual.classroom.entity.StudentSession;

/**
 * @author Dragan
 *
 */
@Stateless
public class StudentSessionDAO extends GenericDAO<StudentSession, Integer> {

	protected StudentSessionDAO() {
		super(StudentSession.class);
	}

	@SuppressWarnings("unchecked")
	public StudentSession findActive(Integer studentId) {
		Query query = getEntityManager().createNamedQuery("StudentSession.findActive");
		query.setParameter("studentId", studentId);

		// should be only one
		List<StudentSession> activeSessions = (List<StudentSession>) query.getResultList();

		if (activeSessions == null || activeSessions.isEmpty()) {
			return null;
		}
		return activeSessions.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public StudentSession validate(String session) {
		Query query = getEntityManager().createNamedQuery("StudentSession.validate");
		query.setParameter("session", session);
		query.setParameter("now", new Date());
		// should be only one
		List<StudentSession> activeSessions = (List<StudentSession>) query.getResultList();

		if (activeSessions == null || activeSessions.isEmpty())
			return null;
			
		return activeSessions.get(0);
	}

}
