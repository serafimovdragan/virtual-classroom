package virtual.classroom.constant;

public class Constants {

	public static final String ROLE_ADMIN = "Admin";
	public static final String ROLE_PROFESSOR = "Professor";
	public static final String ROLE_ASSISTANT = "Assistant";
	public static final String ROLE_STUDENT = "Student";
	
	public enum Role {

		Admin(ROLE_ADMIN), 
		Professor(ROLE_PROFESSOR), 
		Assistant(ROLE_ASSISTANT), 
		Student(ROLE_STUDENT);
		
		String role;
		
		Role(String role) {
			this.role = role;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}
	}
}
