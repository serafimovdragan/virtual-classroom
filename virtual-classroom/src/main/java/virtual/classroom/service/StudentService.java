/**
 * 
 */
package virtual.classroom.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import virtual.classroom.bean.ExaminationBean;
import virtual.classroom.bean.QuestionBean;
import virtual.classroom.bean.SubmittedAnswerBean;
import virtual.classroom.dao.CourseDAO;
import virtual.classroom.dao.ExaminationDAO;
import virtual.classroom.dao.ExaminationInstanceDAO;
import virtual.classroom.dao.StudentDAO;
import virtual.classroom.dao.StudentEnrollmentDAO;
import virtual.classroom.dao.SubmittedAnswerDAO;
import virtual.classroom.dto.ExaminationDTO;
import virtual.classroom.entity.Course;
import virtual.classroom.entity.Examination;
import virtual.classroom.entity.ExaminationInstance;
import virtual.classroom.entity.QuestionInstance;
import virtual.classroom.entity.Student;
import virtual.classroom.entity.StudentEnrollment;
import virtual.classroom.entity.SubmittedAnswer;
import virtual.classroom.util.errorhandling.ErrorCode;
import virtual.classroom.util.errorhandling.RestClientError;

/**
 * @author Dragan
 *
 */
@Stateless
public class StudentService {

	@EJB
	private StudentEnrollmentDAO studentEnrollmentDAO;
	
	@EJB
	private StudentDAO studentDAO;
	
	@EJB
	private CourseDAO courseDAO;
	
	@EJB
	private ExaminationDAO examinationDAO;
	
	@EJB
	private ExaminationDTO examinationDTO;
	
	@EJB
	private SubmittedAnswerDAO submittedAnswerDAO;
	
	@EJB
	private ExaminationInstanceDAO examinationInstanceDAO;
	
	public void enroll(Integer studentId, Integer courseId) throws RestClientError {
		Student student = studentDAO.find(studentId);
		
		if(student == null)
			throw new RestClientError(ErrorCode.NotFound, "Student");
		
		Course course = courseDAO.find(courseId);
		
		if(course == null)
			throw new RestClientError(ErrorCode.NotFound, "Course");
		
		if(student.getCredits() >= course.getCredits()) {
			StudentEnrollment studentEnrollment = new StudentEnrollment();
			
			studentEnrollment.setCourseId(courseId);
			studentEnrollment.setStudentId(studentId);
			
			studentEnrollmentDAO.save(studentEnrollment);
			
			student.setCredits(student.getCredits() - course.getCredits());
		} else 
			throw new RestClientError(ErrorCode.NotEnoughCredits);
	}

	public List<ExaminationBean> getActiveExaminations(Integer studentId) {
		List<Integer> studentEnrollments = studentEnrollmentDAO.getCourseIdsByStudent(studentId);
		List<Examination> activeExaminations = examinationDAO.getByCourses(studentEnrollments);
		List<ExaminationBean> result = new ArrayList<ExaminationBean>();
		
		for (Examination examination : activeExaminations) {
			result.add(examinationDTO.toBean(examination, null));
		}
		
		return result;
	}

	public void startExamination(Integer examinationId, Integer studentId) {
		
	}

	public ExaminationBean getExamination(String name) throws RestClientError {
		ExaminationInstance examinationInstance = examinationInstanceDAO.findByInstance(name);
		
		if(examinationInstance == null) {
			throw new RestClientError(ErrorCode.ExaminationInstanceDoesNotExist, name);
		}
		
		List<Integer> questionIds = new ArrayList<Integer>();
		
		for (QuestionInstance instance : examinationInstance.getQuestionInstances()) {
			questionIds.add(instance.getQuestionId());
		}
		Examination examination = examinationDAO.find(examinationInstance.getExaminationId());
		
		ExaminationBean examinationBean = examinationDTO.toBean(examination, null);
		examinationBean.getQuestions().removeIf((QuestionBean b) -> !questionIds.contains(b.getId()));
		
		for (QuestionBean questionBean : examinationBean.getQuestions()) {
			for (QuestionInstance questionInstance : examinationInstance.getQuestionInstances()) {
				if(questionBean.getId() == questionInstance.getQuestionId() && questionInstance.getExpirationDate().compareTo(new Date()) > 0) {
					questionBean.setExpirationDate(questionInstance.getExpirationDate());
				} else if(questionBean.getId() == questionInstance.getQuestionId()) {
					questionBean.setSeeResults(questionInstance.getSeeResults());
				}
			}
		}
		
		return examinationBean;
		
	}

	public Integer submitExamination(List<SubmittedAnswerBean> submittedAnswerBeans, String ipAdress) throws RestClientError {
		Integer questionId = -1;
		for (SubmittedAnswerBean submittedAnswerBean : submittedAnswerBeans) {
			SubmittedAnswer submittedAnswer = new SubmittedAnswer();
			
			submittedAnswer.setExaminationId(submittedAnswerBean.getExaminationId());
			submittedAnswer.setQuestionId(submittedAnswerBean.getQuestionId());
			submittedAnswer.setAnswerId(submittedAnswerBean.getAnswerId());
			submittedAnswer.setAnswer(submittedAnswerBean.getAnswer());
			submittedAnswer.setQuestionType(submittedAnswerBean.getQuestionType());
			submittedAnswer.setInstance(submittedAnswerBean.getInstance());
			submittedAnswer.setStudentId(submittedAnswerBean.getStudentId());
			submittedAnswer.setIpAdress(ipAdress);
			
			if(submittedAnswerDAO.alreadyAnswered(submittedAnswer)) {
				throw new RestClientError(ErrorCode.AnswerAlreadySubmitted);
			}
			
			submittedAnswerDAO.save(submittedAnswer);

			questionId = submittedAnswer.getQuestionId();
		}

		return questionId;
	}

}
