package virtual.classroom.service;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import virtual.classroom.constant.Constants.Role;
import virtual.classroom.constant.PropertyConstants;
import virtual.classroom.util.errorhandling.RestClientError;


@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

	private static final Response ACCESS_UNAUTHORIZED = Response.status(Response.Status.UNAUTHORIZED).build();
	private static final String AUTHORIZATION_HEADER = "Authorization";
	
	@Context
	private ResourceInfo resourceInfo;

	@EJB
	private AuthenticationService authenticationService;
	
	@EJB
	private PropertyService propertyService;

	public void filter(ContainerRequestContext requestContext) throws IOException {
		Boolean testMode = false;
		try {
			testMode = Boolean.valueOf(propertyService.getProperty(PropertyConstants.TEST_MODE).getValue());
		} catch (RestClientError e) {
			System.out.println(e.getMessage());
		}
		String session = requestContext.getHeaderString(AUTHORIZATION_HEADER);

		Method method = resourceInfo.getResourceMethod();

		try {
			if (method.isAnnotationPresent(RolesAllowed.class)) {
				RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
				String[] rolesStrings = rolesAnnotation.value();
				Role role = null;
				for (String roleString : rolesStrings) {
					role = Role.valueOf(roleString);
				}
				if(!testMode && (role == null || !session.contains(role.getRole()) || !authenticationService.validate(session))) {
					requestContext.abortWith(ACCESS_UNAUTHORIZED);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			requestContext.abortWith(ACCESS_UNAUTHORIZED);
		}
	}

}
