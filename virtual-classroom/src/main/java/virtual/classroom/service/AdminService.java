/**
 * 
 */
package virtual.classroom.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import virtual.classroom.bean.AdminBean;
import virtual.classroom.bean.CourseBean;
import virtual.classroom.bean.ProfessorBean;
import virtual.classroom.bean.StudentBean;
import virtual.classroom.dao.AdminDAO;
import virtual.classroom.dao.CourseDAO;
import virtual.classroom.dao.ProfessorDAO;
import virtual.classroom.dao.StudentDAO;
import virtual.classroom.dto.AdminDTO;
import virtual.classroom.dto.CourseDTO;
import virtual.classroom.dto.ProfessorDTO;
import virtual.classroom.dto.StudentDTO;
import virtual.classroom.entity.Admin;
import virtual.classroom.entity.Course;
import virtual.classroom.entity.Professor;
import virtual.classroom.entity.Student;
import virtual.classroom.util.errorhandling.AuthenticationError;
import virtual.classroom.util.errorhandling.ErrorCode;

/**
 * @author Dragan
 *
 */
@Stateless
public class AdminService {

	@EJB
	private AuthenticationService authenticationService;
	
	@EJB
	private AdminDAO adminDAO;
	
	@EJB
	private ProfessorDAO professorDAO;
	
	@EJB
	private StudentDAO studentDAO;
	
	@EJB
	private AdminDTO adminDTO;
	
	@EJB
	private ProfessorDTO professorDTO;
	
	@EJB
	private StudentDTO studentDTO;
	
	@EJB
	private CourseDAO courseDAO;
	
	@EJB
	private CourseDTO courseDTO;
	
	public void insertProfessor(ProfessorBean professorBean) throws NoSuchAlgorithmException, InvalidKeySpecException, AuthenticationError {
		Professor professor = professorDTO.toEntity(professorBean, null);
	
		String hash = authenticationService.generatePasswordHash(professorBean.getPassword());
		
		String[] hashParts = hash.split(":");
		
		professor.setPassword(hashParts[0]);
		professor.setSalt(hashParts[1]);
		
		try {
			professorDAO.save(professor).getId();
		} catch(Exception e) {
			throw new AuthenticationError(ErrorCode.UserExists, professorBean.getUsername());
		}
	}

	public void editProfessor(ProfessorBean professorBean) throws AuthenticationError {
		Professor professor = professorDAO.find(professorBean.getId());
		
		if(professor == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Professor");
		
		professor = professorDTO.toEntity(professorBean, professor);
		
		professorDAO.update(professor).getId();
	}

	public void deleteProfessor(Integer professorId) {
		professorDAO.delete(professorId);
	}

	public List<ProfessorBean> getprofessor(Integer professorId) {
		List<ProfessorBean> result = new ArrayList<ProfessorBean>();
		if(professorId != null) {
			ProfessorBean professorBean = professorDTO.toBean(professorDAO.find(professorId), null);
			
			if(professorBean != null)
				result.add(professorBean);
			
			return result;
		}
		
		Collection<Professor> allProfessors = professorDAO.findAll(0, 0);
		
		for (Professor professor : allProfessors) {
			result.add(professorDTO.toBean(professor, null));
		}
		
		return result;
	}
	
	public void insertStudent(StudentBean studentBean) throws NoSuchAlgorithmException, InvalidKeySpecException {
		Student student = studentDTO.toEntity(studentBean, null);
		
		String hash = authenticationService.generatePasswordHash(studentBean.getPassword());
		
		String[] hashParts = hash.split(":");
		
		student.setPassword(hashParts[0]);
		student.setSalt(hashParts[1]);
		
		studentDAO.save(student).getId();
	}

	public void editStudent(StudentBean studentBean) throws AuthenticationError {
		Student student = studentDAO.find(studentBean.getId());
		
		if(student == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Student");
		
		student = studentDTO.toEntity(studentBean, student);
		
		studentDAO.update(student).getId();
	}

	public void deleteStudent(Integer studentId) {
		studentDAO.delete(studentId);
	}
	

	public List<StudentBean> getStudent(Integer studentId) {
		List<StudentBean> result = new ArrayList<StudentBean>();
		if(studentId != null) {
			StudentBean studentBean = studentDTO.toBean(studentDAO.find(studentId), null);
			
			if(studentBean != null)
				result.add(studentBean);
			
			return result;
		}
		
		Collection<Student> allStudents = studentDAO.findAll(0, 0);
		
		for (Student student : allStudents) {
			result.add(studentDTO.toBean(student, null));
		}
		
		return result;
	}

	public void insertAdmin(AdminBean adminBean) throws NoSuchAlgorithmException, InvalidKeySpecException {
		Admin admin = adminDTO.toEntity(adminBean, null);
		
		String hash = authenticationService.generatePasswordHash(adminBean.getPassword());
		
		String[] hashParts = hash.split(":");
		
		admin.setPassword(hashParts[0]);
		admin.setSalt(hashParts[1]);
		
		adminDAO.save(admin);
	}

	public void editAdmin(AdminBean adminBean) throws AuthenticationError {
		Admin admin = adminDAO.find(adminBean.getId());
		
		if(admin == null) 
			throw new AuthenticationError(ErrorCode.NotFound, "Admin");
		
		admin = adminDTO.toEntity(adminBean, admin);
		
		adminDAO.update(admin);
	}
	
	public void deleteAdmin(Integer adminId) {
		adminDAO.delete(adminId);
	}

	public List<AdminBean> getAdmin(Integer adminId) {
		List<AdminBean> result = new ArrayList<AdminBean>();
		if(adminId != null) {
			AdminBean adminBean = adminDTO.toBean(adminDAO.find(adminId), null);
			
			if(adminBean != null)
				result.add(adminBean);
			
			return result;
		}
		
		Collection<Admin> allAdmins = adminDAO.findAll(0, 0);
		
		for (Admin admin : allAdmins) {
			result.add(adminDTO.toBean(admin, null));
		}
		
		return result;
	}

	public void insertCourse(CourseBean courseBean) {
		Course course = courseDTO.toEntity(courseBean, null);
		courseDAO.save(course);
	}

	public void editCourse(CourseBean courseBean) throws AuthenticationError {
		Course course = courseDAO.find(courseBean.getId());
		
		if(course == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Course");
		
		course = courseDTO.toEntity(courseBean, course);
		
		courseDAO.update(course);
	}

	public void deleteCourse(Integer courseId) {
		courseDAO.delete(courseId);
	}

	public List<CourseBean> getCourse(Integer courseId) {
		List<CourseBean> result = new ArrayList<CourseBean>();
		if(courseId != null) {
			CourseBean courseBean = courseDTO.toBean(courseDAO.find(courseId), null);
			
			if(courseBean != null)
				result.add(courseBean);
			
			return result;
		}
		
		Collection<Course> allCourses = courseDAO.findAll(0, 0);
		
		for (Course course : allCourses) {
			result.add(courseDTO.toBean(course, null));
		}
		
		return result;
	}

}
