/**
 * 
 */
package virtual.classroom.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.tools.ant.util.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import virtual.classroom.bean.AnswerBean;
import virtual.classroom.bean.ExaminationBean;
import virtual.classroom.bean.ExaminationInstanceBean;
import virtual.classroom.bean.QuestionBean;
import virtual.classroom.bean.QuestionInstanceBean;
import virtual.classroom.bean.SubmittedAnswerBean;
import virtual.classroom.dao.AnswerDAO;
import virtual.classroom.dao.ExaminationDAO;
import virtual.classroom.dao.ExaminationInstanceDAO;
import virtual.classroom.dao.QuestionDAO;
import virtual.classroom.dao.QuestionInstanceDAO;
import virtual.classroom.dao.SubmittedAnswerDAO;
import virtual.classroom.dto.AnswerDTO;
import virtual.classroom.dto.ExaminationDTO;
import virtual.classroom.dto.ExaminationInstanceDTO;
import virtual.classroom.entity.Answer;
import virtual.classroom.entity.Examination;
import virtual.classroom.entity.Examination.ExaminationType;
import virtual.classroom.entity.ExaminationInstance;
import virtual.classroom.entity.Question;
import virtual.classroom.entity.QuestionInstance;
import virtual.classroom.entity.SubmittedAnswer;
import virtual.classroom.entity.Question.QuestionType;
import virtual.classroom.util.errorhandling.ErrorCode;
import virtual.classroom.util.errorhandling.RestClientError;

/**
 * @author Dragan
 *
 */
@Stateless
public class ExaminationService {
	
	public static final String JBOSS_DATA = "{jboss.server.data.dir}";
	public static final String PICTURES_FOLDER = "pictures";

	@EJB
	private ExaminationDAO examinationDAO;
	
	@EJB
	private ExaminationDTO examinationDTO;
	
	@EJB
	private QuestionDAO questionDAO;
	
	@EJB
	private AnswerDAO answerDAO;
	
	@EJB
	private AnswerDTO answerDTO;
	
	@EJB
	private ExaminationInstanceDAO examinationInstanceDAO;
	
	@EJB
	private QuestionInstanceDAO questionInstanceDAO;
	
	@EJB
	private ExaminationInstanceDTO examinationInstanceDTO;
	
	@EJB
	private SubmittedAnswerDAO submittedAnswerDAO;
	
	public void insertExamination(ExaminationBean examinationBean) throws RestClientError {
		Examination examination = examinationDTO.toEntity(examinationBean, null);
		
		examination.setCreationDate(new Date());
		
		if(examination != null) {
		    Examination checkIfExists = examinationDAO.find(examination.getName());
		    if(checkIfExists != null) {
		        examinationDAO.deleteExaminationByName(checkIfExists.getName());
            }
			examinationDAO.save(examination);
			System.out.println("exam-name: " + examination.getName());

			for (Question question : examination.getQuestions()) {
				if(examination.getType() == ExaminationType.Test) {
					isTestQuestionValid(question);
				} else if(examination.getType() == ExaminationType.Survey) {
					isSurveyQuestionValid(question);
				}
				
				question.setExaminationId(examination.getId());
				questionDAO.save(question);
				
				for (Answer answer : question.getAnswers()) {
					answer.setQuestionId(question.getId());
					answerDAO.save(answer);
				}
			}
		}
	}
	
	public void isSurveyQuestionValid(Question question) throws RestClientError {
		if(question.getType() == QuestionType.Checkbox) {
			
		} else if(question.getType() == QuestionType.Radio) {
			
		} else if(question.getType() == QuestionType.Fill) {
			
		}
	}
	
	public void isTestQuestionValid(Question question) throws RestClientError {
		if(question.getType() == QuestionType.Checkbox) {
			
		} else if(question.getType() == QuestionType.Radio) {
			Iterator<Answer> iterator = question.getAnswers().iterator();
			int correctAnswers = 0;
			
			while(iterator.hasNext()) {
				Answer answer = iterator.next();
				
				if(answer.getIsCorrect()) {
					correctAnswers++;
				}
			}
			
			if(correctAnswers != 1) {
				throw new RestClientError(ErrorCode.MustHaveOneCorrectAnswer, question.getQuestion());
			}
		} else if(question.getType() == QuestionType.Fill) {
			if(question.getAnswers().size() != 1) {
				throw new RestClientError(ErrorCode.CanNotHaveMoreThanOneAnswer, question.getQuestion());
			}
			Answer answer = question.getAnswers().iterator().next();
			if(!answer.getIsCorrect()) {
				throw new RestClientError(ErrorCode.NoCorrectAnswer, question.getQuestion());
			}
		}
	}
	
	public ExaminationBean getExamination(Integer examinationId) {
		return examinationDTO.toBean(examinationDAO.find(examinationId), null);
	}

	public void activateExamination(Integer professorId, Integer examinationId, String instance) throws RestClientError {
		Examination examination = examinationDAO.find(examinationId);
		
		if(examination == null)
			throw new RestClientError(ErrorCode.NotFound, "Examination");
		
		ExaminationInstance examinationInstance = new ExaminationInstance();
		
		examinationInstance.setExaminationId(examinationId);
		examinationInstance.setName(instance);
		examinationInstance.setProfessorId(professorId);
		
		try {
			examinationInstanceDAO.save(examinationInstance);
		} catch (Exception e) {
			throw new RestClientError(ErrorCode.InstanceExists, examinationId, instance);
		}
	}

	public void activateQuestion(Integer examinationId, String instance, Integer questionId, Integer duration, Boolean seeResults) throws RestClientError {
		ExaminationInstance examinationInstance = examinationInstanceDAO.findByInstance(examinationId, instance);
		
		if(examinationInstance == null)
			throw new RestClientError(ErrorCode.NotFound, "Examination Instance");
		
		Question question = questionDAO.find(questionId);
		
		if(question == null)
			throw new RestClientError(ErrorCode.NotFound, "Question");
		
		QuestionInstance questionInstance = new QuestionInstance();
		questionInstance.setExaminationInstanceId(examinationInstance.getId());
		questionInstance.setQuestionId(questionId);
		questionInstance.setDuration(duration);
		questionInstance.setSeeResults(seeResults);
		questionInstance.setActivationDate(new Date());
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.SECOND, duration);
		
		questionInstance.setExpirationDate(c.getTime());
		
		try {
			questionInstanceDAO.save(questionInstance);
		} catch (Exception e) {
			throw new RestClientError(ErrorCode.QuestionInstanceExists, questionId, instance);
		}
		
	}

	public List<ExaminationInstanceBean> getExaminationInstances(Integer examinationId, Integer professorId) {
		List<ExaminationInstance> examinationInstances = examinationInstanceDAO.findByProfessor(examinationId, professorId);
		List<ExaminationInstanceBean> examinationInstanceBeans = new ArrayList<ExaminationInstanceBean>();
		
		for (ExaminationInstance examinationInstance : examinationInstances) {
			examinationInstanceBeans.add(examinationInstanceDTO.toBean(examinationInstance, null));
		}
		
		return examinationInstanceBeans;
	}

	public ExaminationInstanceBean getExaminationInstance(Integer examinationId, String instance) {
		return examinationInstanceDTO.toBean(examinationInstanceDAO.findByInstance(examinationId, instance), null);
	}

	public QuestionBean getResults(String instance, Integer questionId) {
		QuestionBean questionBean = new QuestionBean();
		List<Answer> answers = answerDAO.getAnswers(questionId);
		
		for (Answer answer : answers) {
			AnswerBean answerBean = answerDTO.toBean(answer, null);
			
			questionBean.getAnswers().add(answerBean);
		}
		
		List<SubmittedAnswer> submittedAnswers = submittedAnswerDAO.getAnswers(instance, questionId);
		
		for (SubmittedAnswer submittedAnswer : submittedAnswers) {
			if(questionBean.getType() == null) {
				questionBean.setType(submittedAnswer.getQuestionType());
			}
			
			SubmittedAnswerBean submittedAnswerBean = new SubmittedAnswerBean();
			submittedAnswerBean.setStudentId(submittedAnswer.getStudentId());
			submittedAnswerBean.setExaminationId(submittedAnswer.getExaminationId());
			submittedAnswerBean.setQuestionId(submittedAnswer.getQuestionId());
			submittedAnswerBean.setAnswerId(submittedAnswer.getAnswerId());
			submittedAnswerBean.setAnswer(submittedAnswer.getAnswer());
			
			questionBean.getSubmittedAnswers().add(submittedAnswerBean);
		}
		return questionBean;
	}

	public String generateRandomString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 18) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	public String uploadPicture(MultipartFormDataInput picture) throws RestClientError {
		List<InputPart> inPart = picture.getFormDataMap().get("picture");
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		for (InputPart inputPart : inPart) {
			try {
				inputStream = inputPart.getBody(InputStream.class, null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		String picturePath = PICTURES_FOLDER + "/" + generateRandomString() + ".png";
		try {
			outputStream = new FileOutputStream(new File(picturePath));
			IOUtils.copy(inputStream, outputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return picturePath;
	}

	public void publishQuestion(String instance, Integer questionId) {
		ExaminationInstance examination = examinationInstanceDAO.findByInstance(instance);
		
		for (QuestionInstance question : examination.getQuestionInstances()) {
			if(question.getQuestionId().equals(questionId)) {
				question.setSeeResults(true);
				break;
			}
		}
	}

	public QuestionInstanceBean isQuestionPublic(String instance, Integer questionId) {
		ExaminationInstance examination = examinationInstanceDAO.findByInstance(instance);
		QuestionInstanceBean questionBean = new QuestionInstanceBean();
		
		for (QuestionInstance question : examination.getQuestionInstances()) {
			if(question.getQuestionId().equals(questionId)) {
				questionBean.setId(questionId);
				questionBean.setSeeResults(question.getSeeResults());
			}
		}
		return questionBean;
	}

	public void deleteExamination(Integer id) {
		examinationDAO.deleteExamination(id);
	}
}
