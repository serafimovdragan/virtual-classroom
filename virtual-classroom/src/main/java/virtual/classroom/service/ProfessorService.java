/**
 * 
 */
package virtual.classroom.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import virtual.classroom.bean.CourseBean;
import virtual.classroom.bean.ExaminationBean;
import virtual.classroom.dao.CourseDAO;
import virtual.classroom.dao.ExaminationDAO;
import virtual.classroom.dto.CourseDTO;
import virtual.classroom.dto.ExaminationDTO;
import virtual.classroom.entity.Course;
import virtual.classroom.entity.Examination;

/**
 * @author Dragan
 *
 */
@Stateless
public class ProfessorService {

	@EJB
	private CourseDAO courseDAO;
	
	@EJB
	private CourseDTO courseDTO;
	
	@EJB
	private ExaminationDAO examinationDAO;
	
	@EJB
	private ExaminationDTO examinationDTO;
	
	public List<CourseBean> getCourses(Integer professorId) {
		List<Course> courses = courseDAO.findByProfessor(professorId);
		List<CourseBean> result = new ArrayList<CourseBean>();
		
		for (Course course : courses) {
			result.add(courseDTO.toBean(course, null));
		}
		
		return result;
	}

	public List<ExaminationBean> getExaminations(Integer courseId) {
		List<Integer> courseIds = new ArrayList<Integer>();
		courseIds.add(courseId);
		
		List<Examination> examinations = examinationDAO.getByCourses(courseIds);
		List<ExaminationBean> result = new ArrayList<ExaminationBean>();
		
		for (Examination examination : examinations) {
			result.add(examinationDTO.toBean(examination, null));
		}
		
		return result;
	}

}
