/**
 * 
 */
package virtual.classroom.service;

import java.security.Principal;
import java.security.acl.Group;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;

import org.jboss.security.auth.spi.AbstractServerLoginModule;

/**
 * @author Dragan
 *
 */
public class ServerLoginModule extends AbstractServerLoginModule {

	private Principal identity;
	
	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
		System.out.println("a");
		super.initialize(subject, callbackHandler, sharedState, options);
	}
	
	@Override
	public boolean login() throws LoginException {
		System.out.println("b");
		return super.login();
	}

	@Override
	protected Principal getIdentity() {
		System.out.println("c");
		return identity;
	}

	@Override
	protected Group[] getRoleSets() throws LoginException {
		System.out.println("d");
		return null;
	}

}
