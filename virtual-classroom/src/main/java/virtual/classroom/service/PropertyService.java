/**
 * 
 */
package virtual.classroom.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import virtual.classroom.bean.ApplicationPropertyBean;
import virtual.classroom.util.configuration.Configuration;
import virtual.classroom.util.errorhandling.ErrorCode;
import virtual.classroom.util.errorhandling.RestClientError;

/**
 * @author Dragan
 *
 */
@Stateless
public class PropertyService {
	
	@EJB
	private Configuration configuration;

	public ApplicationPropertyBean getProperty(String key) throws RestClientError {
		String value = configuration.getProperties().getProperty(key);
		
		if(value == null) {
			throw new RestClientError(ErrorCode.PropertyDoesNotExist, key);
		}
		ApplicationPropertyBean applicationPropertyBean = new ApplicationPropertyBean();
		
		applicationPropertyBean.setKey(key);
		applicationPropertyBean.setValue(value);
		
		return applicationPropertyBean;
	}

	public List<ApplicationPropertyBean> getProperties() throws RestClientError {
		List<ApplicationPropertyBean> applicationPropertyBeans = new ArrayList<ApplicationPropertyBean>();
		
		Iterator<Object> iterator = configuration.getProperties().keySet().iterator();
		
		while(iterator.hasNext()) {
			
			String key = iterator.next().toString();
			
			applicationPropertyBeans.add(getProperty(key));
		}
		
		return applicationPropertyBeans;
	}

	public void putProperty(String key, String value) {
		configuration.putProperty(key, value);
	}
}
