/**
 * 
 */
package virtual.classroom.service;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import virtual.classroom.bean.AdminBean;
import virtual.classroom.bean.AdminSessionBean;
import virtual.classroom.bean.ProfessorBean;
import virtual.classroom.bean.ProfessorSessionBean;
import virtual.classroom.bean.StudentBean;
import virtual.classroom.bean.StudentSessionBean;
import virtual.classroom.dao.AdminDAO;
import virtual.classroom.dao.AdminSessionDAO;
import virtual.classroom.dao.ProfessorDAO;
import virtual.classroom.dao.ProfessorSessionDAO;
import virtual.classroom.dao.StudentDAO;
import virtual.classroom.dao.StudentSessionDAO;
import virtual.classroom.dto.AdminSessionDTO;
import virtual.classroom.dto.ProfessorSessionDTO;
import virtual.classroom.dto.StudentDTO;
import virtual.classroom.dto.StudentSessionDTO;
import virtual.classroom.entity.Admin;
import virtual.classroom.entity.AdminSession;
import virtual.classroom.entity.Professor;
import virtual.classroom.entity.ProfessorSession;
import virtual.classroom.entity.Student;
import virtual.classroom.entity.StudentSession;
import virtual.classroom.util.errorhandling.AuthenticationError;
import virtual.classroom.util.errorhandling.ErrorCode;

/**
 * @author Dragan
 *
 */
@Stateless
public class AuthenticationService {

	public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";
	
	public static final int BYTES = 32;
	
	public static final int ITERATIONS = 1000;
	
	@EJB
	private ProfessorDAO professorDAO;
	
	@EJB
	private ProfessorSessionDAO professorSessionDAO;
	
	@EJB
	private AdminDAO adminDAO;
	
	@EJB
	private StudentDAO studentDAO;
	
	@EJB
	private AdminSessionDAO adminSessionDAO;
	
	@EJB
	private StudentSessionDAO studentSessionDAO;
	
	@EJB
	private ProfessorSessionDTO professorSessionDTO;
	
	@EJB
	private AdminSessionDTO adminSessionDTO;
	
	@EJB
	private StudentDTO studentDTO;
	
	@EJB
	private StudentSessionDTO studentSessionDTO;

	public ProfessorSessionBean professorLogin(ProfessorBean professorBean) throws AuthenticationError, NoSuchAlgorithmException, InvalidKeySpecException, ParseException {
		Professor professor = professorDAO.find(professorBean.getUsername());
		
		if(professor == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Professor");
		
		if(!checkPassword(professor.getPassword(), professor.getSalt(), professorBean.getPassword()))
			throw new AuthenticationError(ErrorCode.PasswordIncorrect);
		
		ProfessorSession professorSession = professorSessionDAO.findActive(professor.getId());
		
		if(professorSession != null) {
			extendProfessorSession(professorSession);
			return professorSessionDTO.toBean(professorSession, null);
		}
		
		professorSession = new ProfessorSession();
		
		professorSession.setProfessorId(professor.getId());
		
		UUID session = UUID.randomUUID();
		professorSession.setSession(professor.getType() + ":" + session.toString());
		
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getDefault());

		professorSession.setCreationDate(c.getTime());
		
		c.add(Calendar.HOUR, 1);
		
		professorSession.setExpirationDate(c.getTime());
		
		professorSessionDAO.save(professorSession);
		
		return professorSessionDTO.toBean(professorSession, null);
	}
	

	public AdminSessionBean adminLogin(AdminBean adminBean) throws AuthenticationError, NoSuchAlgorithmException, InvalidKeySpecException {
		Admin admin = adminDAO.find(adminBean.getUsername());
		
		if(admin == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Admin");
		
		if(!checkPassword(admin.getPassword(), admin.getSalt(), adminBean.getPassword()))
			throw new AuthenticationError(ErrorCode.PasswordIncorrect);
		
		AdminSession adminSession = adminSessionDAO.findActive(admin.getId());
		
		if(adminSession != null) {
			extendAdminSession(adminSession);
			return adminSessionDTO.toBean(adminSession, null);
		}
		
		adminSession = new AdminSession();
		
		adminSession.setAdminId(admin.getId());
		
		UUID session = UUID.randomUUID();
		adminSession.setSession("Admin:" + session.toString());
		
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getDefault());

		adminSession.setCreationDate(c.getTime());
		
		c.add(Calendar.HOUR, 1);
		
		adminSession.setExpirationDate(c.getTime());
		
		adminSessionDAO.save(adminSession);
		
		return adminSessionDTO.toBean(adminSession, null);
	}
	
	public StudentSessionBean studentLogin(StudentBean studentBean) throws AuthenticationError, NoSuchAlgorithmException, InvalidKeySpecException {
		Student student = studentDAO.find(studentBean.getUsername());
		
		if(student == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Student");
		
		if(!checkPassword(student.getPassword(), student.getSalt(), studentBean.getPassword()))
			throw new AuthenticationError(ErrorCode.PasswordIncorrect);
		
		StudentSession studentSession = studentSessionDAO.findActive(student.getId());
		
		if(studentSession != null) {
			extendStudentSession(studentSession);
			return studentSessionDTO.toBean(studentSession, null);
		}
		
		studentSession = new StudentSession();
		
		studentSession.setStudentId(student.getId());
		
		UUID session = UUID.randomUUID();
		studentSession.setSession("Student:" + session.toString());
		
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getDefault());

		studentSession.setCreationDate(c.getTime());
		
		c.add(Calendar.HOUR, 1);
		
		studentSession.setExpirationDate(c.getTime());

		studentSessionDAO.save(studentSession);
		
		return studentSessionDTO.toBean(studentSession, null);
	}
	

	public void adminLogout(String session) throws AuthenticationError {
		AdminSession adminSession = adminSessionDAO.validate(session);
		
		if(adminSession == null) 
			throw new AuthenticationError(ErrorCode.NotFound, "Admin Session");
		
		adminSession.setExpirationDate(new Date());
	}
	
	public void professorLogout(String session) throws AuthenticationError {
		ProfessorSession professorSession = professorSessionDAO.validate(session);
		
		if(professorSession == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Professor Session");
		
		professorSession.setExpirationDate(new Date());
	}

	public void studentLogout(String session) throws AuthenticationError {
		StudentSession studentSession = studentSessionDAO.validate(session);
		
		if(studentSession == null)
			throw new AuthenticationError(ErrorCode.NotFound, "Student Session");
		
		studentSession.setExpirationDate(new Date());
	}
	
	private void extendProfessorSession(ProfessorSession professorSession) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR, 1);

		professorSession.setExpirationDate(c.getTime());
	}

	private void extendAdminSession(AdminSession adminSession) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR, 1);

		adminSession.setExpirationDate(c.getTime());
	}
	
	private void extendStudentSession(StudentSession studentSession) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR, 1);

		studentSession.setExpirationDate(c.getTime());
	}

	public Boolean validate(String session) {
		if(session.contains("Admin")) {
			AdminSession adminSession = adminSessionDAO.validate(session);
			
			if(adminSession == null)
				return false;
			
			extendAdminSession(adminSession);
			return true;
		} else if(session.contains("Student")) {
			StudentSession studentSession = studentSessionDAO.validate(session);
			
			if(studentSession == null)
				return false;
			
			extendStudentSession(studentSession);
			return true;
		} else if(session.contains("Professor") || session.contains("Assistant")){
			ProfessorSession professorSession = professorSessionDAO.validate(session);
			if(professorSession == null)
				return false;
			
			extendProfessorSession(professorSession);
			return true;
		}
		return false;
	}
	
	public boolean checkPassword(String password, String saltString, String insertedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] salt = fromHex(saltString);
		
		byte[] hash = pbkdf2(insertedPassword.toCharArray(), salt, ITERATIONS, BYTES);
		
		String hashString  = toHex(hash);
		
		return hashString.equals(password);
	}
	
	public String generatePasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		SecureRandom secureRandom = new SecureRandom();
		
		byte[] salt = new byte[BYTES];
		secureRandom.nextBytes(salt);
		
		byte[] hash = pbkdf2(password.toCharArray(), salt, ITERATIONS, BYTES);
		
		return toHex(hash) + ":" + toHex(salt);
	}
	
	private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
		return skf.generateSecret(spec).getEncoded();
	}
	
	private static String toHex(byte[] array) {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0)
			return String.format("%0" + paddingLength + "d", 0) + hex;
		else
			return hex;
	}

	private static byte[] fromHex(String hex) {
		byte[] binary = new byte[hex.length() / 2];
		for (int i = 0; i < binary.length; i++) {
			binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return binary;
	}
}
