/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.ProfessorBean;
import virtual.classroom.entity.Professor;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class ProfessorDTO implements IDTO<Professor, ProfessorBean> {

	public Professor toEntity(ProfessorBean bean, Professor entity) {
		if(bean == null)
			return null;
		
		if(entity == null)
			entity = new Professor();
		
		if(bean.getUsername() != null)
			entity.setUsername(bean.getUsername());
		
		if(bean.getName() != null)
			entity.setName(bean.getName());
		
		if(bean.getSurname() != null)
			entity.setSurname(bean.getSurname());
		
		if(bean.getType() != null)
			entity.setType(bean.getType());
		
		return entity;
	}

	public ProfessorBean toBean(Professor entity, ProfessorBean bean) {
		if(entity == null)
			return null;
		
		if(bean == null)
			bean = new ProfessorBean();
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getUsername() != null)
			bean.setUsername(entity.getUsername());
		
		if(entity.getName() != null)
			bean.setName(entity.getName());
		
		if(entity.getSurname() != null)
			bean.setSurname(entity.getSurname());
		
		if(entity.getType() != null)
			bean.setType(entity.getType());
		
		if(entity.getCreationDate() != null)
			bean.setCreationDate(entity.getCreationDate());
		
		if(entity.getLastActivity() != null)
			bean.setLastActivity(entity.getLastActivity());
		
		return bean;
	}

}
