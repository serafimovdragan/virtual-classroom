/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.AdminSessionBean;
import virtual.classroom.entity.AdminSession;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class AdminSessionDTO implements IDTO<AdminSession, AdminSessionBean> {

	public AdminSession toEntity(AdminSessionBean bean, AdminSession entity) {
		if(bean == null)
			return null;
		
		if(entity == null)
			entity = new AdminSession();
		
		if(bean.getAdminId() != null)
			entity.setAdminId(bean.getAdminId());
		
		if(bean.getCreationDate() != null)
			entity.setCreationDate(bean.getCreationDate());
		
		if(bean.getExpirationDate() != null)
			entity.setExpirationDate(bean.getExpirationDate());
		
		if(bean.getSession() != null)
			entity.setSession(bean.getSession());
		
		return entity;
	}

	public AdminSessionBean toBean(AdminSession entity, AdminSessionBean bean) {
		if(entity == null)
			return null;
		
		if(bean == null)
			bean = new AdminSessionBean();
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getAdminId() != null)
			bean.setAdminId(entity.getAdminId());
		
		if(entity.getSession() != null)
			bean.setSession(entity.getSession());
		
		if(entity.getCreationDate() != null)
			bean.setCreationDate(entity.getCreationDate());
		
		if(entity.getExpirationDate() != null)
			bean.setExpirationDate(entity.getExpirationDate());
		
		return bean;
	}

}
