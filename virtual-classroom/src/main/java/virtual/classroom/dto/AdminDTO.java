package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.AdminBean;
import virtual.classroom.entity.Admin;

@Stateless
@LocalBean
@Remote(IDTO.class)
public class AdminDTO implements IDTO<Admin, AdminBean> {

	public Admin toEntity(AdminBean bean, Admin entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new Admin();
		}
		
		if(bean.getUsername() != null)
			entity.setUsername(bean.getUsername());
		
		if(bean.getName() != null)
			entity.setName(bean.getName());
		
		if(bean.getSurname() != null)
			entity.setSurname(bean.getUsername());
		
		return entity;
	}

	public AdminBean toBean(Admin entity, AdminBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new AdminBean();
		}
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getUsername() != null)
			bean.setUsername(entity.getUsername());
		
		if(entity.getPassword() != null)
			bean.setPassword(entity.getPassword());
		
		if(entity.getSalt() != null)
			bean.setSalt(entity.getSalt());
		
		if(entity.getName() != null)
			bean.setName(entity.getName());
		
		if(entity.getSurname() != null)
			bean.setSurname(entity.getSurname());
		
		return bean;
	}

}
