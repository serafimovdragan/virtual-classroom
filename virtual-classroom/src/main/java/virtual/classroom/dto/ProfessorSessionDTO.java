package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.ProfessorSessionBean;
import virtual.classroom.entity.ProfessorSession;

@Stateless
@LocalBean
@Remote(IDTO.class)
public class ProfessorSessionDTO implements IDTO<ProfessorSession, ProfessorSessionBean> {

	public ProfessorSession toEntity(ProfessorSessionBean bean, ProfessorSession entity) {
		if(bean == null)
			return null;
		
		if(entity == null)
			entity = new ProfessorSession();
		
		if(bean.getProfessorId() != null)
			entity.setProfessorId(bean.getProfessorId());
		
		if(bean.getCreationDate() != null)
			entity.setCreationDate(bean.getCreationDate());
		
		if(bean.getExpirationDate() != null)
			entity.setExpirationDate(bean.getExpirationDate());
		
		if(bean.getSession() != null)
			entity.setSession(bean.getSession());
		
		return entity;
	}

	public ProfessorSessionBean toBean(ProfessorSession entity, ProfessorSessionBean bean) {
		if(entity == null)
			return null;
		
		if(bean == null)
			bean = new ProfessorSessionBean();
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getProfessorId() != null)
			bean.setProfessorId(entity.getProfessorId());
		
		if(entity.getSession() != null)
			bean.setSession(entity.getSession());
		
		if(entity.getCreationDate() != null)
			bean.setCreationDate(entity.getCreationDate());
		
		if(entity.getExpirationDate() != null)
			bean.setExpirationDate(entity.getExpirationDate());
		
		return bean;
	}
}
