/**
 * 
 */
package virtual.classroom.dto;

import java.util.Iterator;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.AnswerBean;
import virtual.classroom.bean.QuestionBean;
import virtual.classroom.entity.Answer;
import virtual.classroom.entity.Question;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class QuestionDTO implements IDTO<Question, QuestionBean> {

	@EJB
	private AnswerDTO answerDTO;
	
	public Question toEntity(QuestionBean bean, Question entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new Question();
		}
		
		if(bean.getExaminationId() != null)
			entity.setExaminationId(bean.getExaminationId());
		
		if(bean.getQuestion() != null)
			entity.setQuestion(bean.getQuestion());

		if(bean.getPictureURL() != null)
			entity.setPictureURL(bean.getPictureURL());
		
		if(bean.getType() != null) {
			entity.setType(bean.getType());
		}
		
		if(bean.getDuration() != null) {
			entity.setDuration(bean.getDuration());
		}
		
		entity.setActive(bean.isActive());
		
		if(bean.getAnswers() != null && !bean.getAnswers().isEmpty()) {
			Iterator<AnswerBean> iterator = bean.getAnswers().iterator();
			
			while(iterator.hasNext()) {
				entity.getAnswers().add(answerDTO.toEntity(iterator.next(), null));
			}
		}
		
		return entity;
	}

	public QuestionBean toBean(Question entity, QuestionBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new QuestionBean();
		}
		
		if(entity.getId() != null) 
			bean.setId(entity.getId());
		
		if(entity.getExaminationId() != null)
			bean.setExaminationId(entity.getExaminationId());
		
		if(entity.getQuestion() != null)
			bean.setQuestion(entity.getQuestion());

		if(entity.getPictureURL() != null)
			bean.setPictureURL(entity.getPictureURL());
		
		if(entity.getType() != null)
			bean.setType(entity.getType());
		
		if(entity.getDuration() != null) {
			bean.setDuration(entity.getDuration());
		}
		
		bean.setActive(entity.isActive());
		
		if(entity.getAnswers() != null && !entity.getAnswers().isEmpty()) {
			Iterator<Answer> iterator = entity.getAnswers().iterator();
			
			while(iterator.hasNext()) {
				bean.getAnswers().add(answerDTO.toBean(iterator.next(), null));
			}
		}
		
		return bean;
	}

}
