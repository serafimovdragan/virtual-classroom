/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.StudentEnrollmentBean;
import virtual.classroom.entity.StudentEnrollment;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class StudentEnrollmentDTO implements IDTO<StudentEnrollment, StudentEnrollmentBean>{

	public StudentEnrollment toEntity(StudentEnrollmentBean bean, StudentEnrollment entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new StudentEnrollment();
		}
		
		if(bean.getCourseId() != null)
			entity.setCourseId(bean.getCourseId());
		
		if(bean.getStudentId() != null)
			entity.setStudentId(bean.getStudentId());
		
		return entity;
	}

	public StudentEnrollmentBean toBean(StudentEnrollment entity, StudentEnrollmentBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new StudentEnrollmentBean();
		}
		
		if(entity.getCourseId() != null)
			bean.setCourseId(entity.getCourseId());
		
		if(entity.getStudentId() != null)
			bean.setStudentId(entity.getStudentId());
		
		return bean;
	}

}
