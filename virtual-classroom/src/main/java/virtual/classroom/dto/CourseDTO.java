/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.CourseBean;
import virtual.classroom.entity.Course;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class CourseDTO implements IDTO<Course, CourseBean> {

	public Course toEntity(CourseBean bean, Course entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new Course();
		}
		
		if(bean.getName() != null)
			entity.setName(bean.getName());
		
		if(bean.getCredits() != null)
			entity.setCredits(bean.getCredits());
		
		if(bean.getProfessorId() != null)
			entity.setProfessorId(bean.getProfessorId());
		
		return entity;
	}

	public CourseBean toBean(Course entity, CourseBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new CourseBean();
		}
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getName() != null)
			bean.setName(entity.getName());
		
		if(entity.getCredits() != null)
			bean.setCredits(entity.getCredits());
		
		if(entity.getProfessorId() != null)
			bean.setProfessorId(entity.getProfessorId());
		
		return bean;
	}

}
