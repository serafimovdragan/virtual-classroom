/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.AnswerBean;
import virtual.classroom.entity.Answer;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class AnswerDTO implements IDTO<Answer, AnswerBean>{

	public Answer toEntity(AnswerBean bean, Answer entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new Answer();
		}
		
		if(bean.getQuestionId() != null)
			entity.setQuestionId(bean.getQuestionId());
		
		if(bean.getAnswer() != null)
			entity.setAnswer(bean.getAnswer());
		
		if(bean.getIsCorrect() != null)
			entity.setIsCorrect(bean.getIsCorrect());
		
		return entity;
	}

	public AnswerBean toBean(Answer entity, AnswerBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new AnswerBean();
		}
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getQuestionId() != null)
			bean.setQuestionId(entity.getQuestionId());
		
		if(entity.getAnswer() != null)
			bean.setAnswer(entity.getAnswer());
		
		if(entity.getIsCorrect() != null)
			bean.setIsCorrect(entity.getIsCorrect());
		
		return bean;
	}

}
