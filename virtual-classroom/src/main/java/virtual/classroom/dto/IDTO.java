package virtual.classroom.dto;

public interface IDTO<E, B> {

	public E toEntity(B bean, E entity);
	
	public B toBean(E entity, B bean);
}
