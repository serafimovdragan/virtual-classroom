/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.StudentSessionBean;
import virtual.classroom.entity.StudentSession;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class StudentSessionDTO implements IDTO<StudentSession, StudentSessionBean> {

	public StudentSession toEntity(StudentSessionBean bean, StudentSession entity) {
		if(bean == null)
			return null;
		
		if(entity == null)
			entity = new StudentSession();
		
		if(bean.getStudentId() != null)
			entity.setStudentId(bean.getStudentId());
		
		if(bean.getCreationDate() != null)
			entity.setCreationDate(bean.getCreationDate());
		
		if(bean.getExpirationDate() != null)
			entity.setExpirationDate(bean.getExpirationDate());
		
		if(bean.getSession() != null)
			entity.setSession(bean.getSession());
		
		return entity;
	}

	public StudentSessionBean toBean(StudentSession entity, StudentSessionBean bean) {
		if(entity == null)
			return null;
		
		if(bean == null)
			bean = new StudentSessionBean();
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getStudentId() != null)
			bean.setStudentId(entity.getStudentId());
		
		if(entity.getSession() != null)
			bean.setSession(entity.getSession());
		
		if(entity.getCreationDate() != null)
			bean.setCreationDate(entity.getCreationDate());
		
		if(entity.getExpirationDate() != null)
			bean.setExpirationDate(entity.getExpirationDate());
		
		return bean;
	}

}
