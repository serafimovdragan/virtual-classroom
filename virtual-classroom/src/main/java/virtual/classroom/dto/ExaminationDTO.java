/**
 * 
 */
package virtual.classroom.dto;

import java.util.Iterator;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.ExaminationBean;
import virtual.classroom.bean.QuestionBean;
import virtual.classroom.entity.Examination;
import virtual.classroom.entity.Question;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class ExaminationDTO implements IDTO<Examination, ExaminationBean> {

	@EJB
	private ProfessorDTO professorDTO;
	
	@EJB
	private QuestionDTO questionDTO;
	
	public Examination toEntity(ExaminationBean bean, Examination entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new Examination();
		}
		
		if(bean.getName() != null)
			entity.setName(bean.getName());
		
		if(bean.getProfessorId() != null)
			entity.setProfessorId(bean.getProfessorId());
		
		if(bean.getProfessor() != null)
			entity.setProfessor(professorDTO.toEntity(bean.getProfessor(), null));
		
		if(bean.getType() != null) 
			entity.setType(bean.getType());
		
		if(bean.getCreationDate() != null)
			entity.setCreationDate(entity.getCreationDate());
		
		if(bean.getCourseId() != null)
			entity.setCourseId(bean.getCourseId());
		
		if(bean.getQuestions() != null && !bean.getQuestions().isEmpty()) {
			Iterator<QuestionBean> iterator = bean.getQuestions().iterator();
			
			while(iterator.hasNext()) {
				entity.getQuestions().add(questionDTO.toEntity(iterator.next(), null));
			}
		}
		
		return entity;
	}

	public ExaminationBean toBean(Examination entity, ExaminationBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new ExaminationBean();
		}
		
		if(entity.getId() != null) {
			bean.setId(entity.getId());
		}
		
		if(entity.getName() != null)
			bean.setName(entity.getName());
		
		if(entity.getProfessor() != null)
			bean.setProfessor(professorDTO.toBean(entity.getProfessor(), null));
		
		if(entity.getType() != null)
			bean.setType(entity.getType());
		
		if(entity.getCreationDate() != null)
			bean.setCreationDate(entity.getCreationDate());
	
		if(entity.getCourseId() != null)
			bean.setCourseId(entity.getCourseId());
		
		if(entity.getQuestions() != null && !entity.getQuestions().isEmpty()) {
			Iterator<Question> iterator = entity.getQuestions().iterator();
			
			while(iterator.hasNext()) {
				bean.getQuestions().add(questionDTO.toBean(iterator.next(), null));
			}
		}
		
		return bean;
	}

}
