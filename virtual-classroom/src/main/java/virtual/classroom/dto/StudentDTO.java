/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.StudentBean;
import virtual.classroom.entity.Student;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class StudentDTO implements IDTO<Student, StudentBean> {

	public Student toEntity(StudentBean bean, Student entity) {
		if(bean == null)
			return null;
		
		if(entity == null)
			entity = new Student();
		
		if(bean.getUsername() != null)
			entity.setUsername(bean.getUsername());
		
		if(bean.getName() != null)
			entity.setName(bean.getName());
		
		if(bean.getSurname() != null)
			entity.setSurname(bean.getSurname());
		
		if(bean.getCredits() != null)
			entity.setCredits(bean.getCredits());

		return entity;
	}

	public StudentBean toBean(Student entity, StudentBean bean) {
		if(entity == null)
			return null;
		
		if(bean == null)
			bean = new StudentBean();
		
		if(entity.getId() != null)
			bean.setId(entity.getId());
		
		if(entity.getUsername() != null)
			bean.setUsername(entity.getUsername());
		
		if(entity.getPassword() != null)
			bean.setPassword(entity.getPassword());
		
		if(entity.getName() != null)
			bean.setName(entity.getName());
		
		if(entity.getSurname() != null) 
			bean.setSurname(entity.getSurname());
		
		if(entity.getCredits() != null)
			bean.setCredits(entity.getCredits());
		
		if(entity.getCreationDate() != null)
			bean.setCreationDate(entity.getCreationDate());
		
		if(entity.getLastActivity() != null)
			bean.setLastActivity(entity.getLastActivity());
		
		return bean;
	}
}
