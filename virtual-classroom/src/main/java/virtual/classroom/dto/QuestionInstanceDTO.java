/**
 * 
 */
package virtual.classroom.dto;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.QuestionInstanceBean;
import virtual.classroom.entity.QuestionInstance;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class QuestionInstanceDTO implements IDTO<QuestionInstance, QuestionInstanceBean> {

	public QuestionInstance toEntity(QuestionInstanceBean bean, QuestionInstance entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new QuestionInstance();
		}
		
		if(bean.getId() != null) {
			entity.setId(bean.getId());
		}
		
		if(bean.getExaminationInstanceId() != null) {
			entity.setExaminationInstanceId(bean.getExaminationInstanceId());
		}
		
		if(bean.getQuestionId() != null) {
			entity.setQuestionId(bean.getQuestionId());
		}
		
		if(bean.getDuration() != null) {
			entity.setDuration(bean.getDuration());
		}
		
		if(bean.getActivationDate() != null) {
			entity.setActivationDate(bean.getActivationDate());
		}
		
		if(bean.getExpirationDate() != null) {
			entity.setExpirationDate(bean.getExpirationDate());
		}
		
		return entity;
	}

	public QuestionInstanceBean toBean(QuestionInstance entity, QuestionInstanceBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new QuestionInstanceBean();
		}
		
		if(entity.getId() != null) {
			bean.setId(entity.getId());
		}
		
		if(entity.getExaminationInstanceId() != null) {
			bean.setExaminationInstanceId(entity.getExaminationInstanceId());
		}
		
		if(entity.getQuestionId() != null) {
			bean.setQuestionId(entity.getQuestionId());
		}
		
		if(entity.getDuration() != null) {
			bean.setDuration(entity.getDuration());
		}
		
		if(entity.getSeeResults() != null) {
			bean.setSeeResults(entity.getSeeResults());
		}
		
		if(entity.getActivationDate() != null) {
			bean.setActivationDate(entity.getActivationDate());
		}
		
		if(entity.getExpirationDate() != null) {
			bean.setExpirationDate(entity.getExpirationDate());
		}
		
		return bean;
	}

}
