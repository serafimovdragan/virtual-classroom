/**
 * 
 */
package virtual.classroom.dto;

import java.util.Iterator;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import virtual.classroom.bean.ExaminationInstanceBean;
import virtual.classroom.bean.QuestionInstanceBean;
import virtual.classroom.entity.ExaminationInstance;
import virtual.classroom.entity.QuestionInstance;

/**
 * @author Dragan
 *
 */
@Stateless
@LocalBean
@Remote(IDTO.class)
public class ExaminationInstanceDTO implements IDTO<ExaminationInstance, ExaminationInstanceBean> {

	@EJB
	private QuestionInstanceDTO questionInstanceDTO;
	
	public ExaminationInstance toEntity(ExaminationInstanceBean bean, ExaminationInstance entity) {
		if(bean == null) {
			return null;
		}
		
		if(entity == null) {
			entity = new ExaminationInstance();
		}
		
		if(bean.getName() != null) {
			entity.setName(bean.getName());
		}
		
		if(bean.getExaminationId() != null) {
			entity.setExaminationId(bean.getExaminationId());
		}
		
		if(bean.getProfesorId() != null) {
			entity.setProfessorId(bean.getProfesorId());
		}
		
		if(bean.getQuestions() != null && !bean.getQuestions().isEmpty()) {
			Iterator<QuestionInstanceBean> iterator = bean.getQuestions().iterator();
			
			while(iterator.hasNext()) {
				entity.getQuestionInstances().add(questionInstanceDTO.toEntity(iterator.next(), null));
			}
		}
		
		return entity;
	}

	public ExaminationInstanceBean toBean(ExaminationInstance entity, ExaminationInstanceBean bean) {
		if(entity == null) {
			return null;
		}
		
		if(bean == null) {
			bean = new ExaminationInstanceBean();
		}
		
		if(entity.getId() != null) {
			bean.setId(entity.getId());
		}
		
		if(entity.getName() != null) {
			bean.setName(entity.getName());
		}
		
		if(entity.getExaminationId() != null) {
			bean.setExaminationId(entity.getExaminationId());
		}
		
		if(entity.getProfessorId() != null) {
			bean.setProfesorId(entity.getProfessorId());
		}
		
		if(entity.getQuestionInstances() != null && !entity.getQuestionInstances().isEmpty()) {
			Iterator<QuestionInstance> iterator = entity.getQuestionInstances().iterator();
			
			while(iterator.hasNext()) {
				bean.getQuestions().add(questionInstanceDTO.toBean(iterator.next(), null));
			}
		}
		
		return bean;
	}

}
